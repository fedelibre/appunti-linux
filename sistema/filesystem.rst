**********
Filesystem
**********

Formattazione, partizioni, etichette
====================================

Di solito conviene usare GParted, facile e intuitivo da usare.  Ma in alcune
occasioni può essere utile o necessario conoscere la linea di comando.

.. NOTE::
   Naturalmente tutti i comandi successivi richiedono privilegi di root (anche
   se non specificato).

Per prima cosa bisogna creare la partizione o le partizioni del device::

   fdisk /dev/sdX

``m`` è il menu dei comandi.  L'uso è intuitivo: basta seguire le opzioni
predefinite e ricordarsi di digitare ``w`` prima di chiudere il programma,
per rendere effettive le modifiche fatte.

Quindi si formatta ciascuna partizione con ``mkfs``.  Qualche esempio::

   mkfs.vfat /dev/sdb1
   mkfs.ext2 /dev/sdb2

Un altro aspetto utile da conoscere sono le etichette, ovvero i nomi usati
per il montaggio del dispositivo: l'etichetta compare in /media e nella
finestra laterale di Nautilus.

La modifica dell'etichetta cambia a seconda del filesystem::

   e2label /dev/partition my_label      # ext2/ext3
   mlabel -i /dev/partition ::my_label  # fat16/fat32
   ntfslabel /dev/partition my_label    # ntfs

Fonte: http://en.kioskea.net/faq/2014-linux-modify-the-partitions-labels

Per avere tutte le informazioni utili su un dispositivo (filesystem, UUID
e etichetta), si usa::

   blkid /dev/sdX



Installazione
-------------

Per un'installazione conviene avere:

- partizione / di almeno 15 GB
- partizione swap del doppio della RAM (ma dipende)
- partizione /home che occupa il resto

Il filesystem deve essere ext4 (eccetto che per la swap).
Se il disco è a stato solido (SSD), conviene disabilitare il journaling (vd sotto).


Gparted live
------------

Per modificare partizioni di un sistema già installato occorre usare una
distribuzione live e in particolare quella specifica per questi compiti,
Gparted live: http://gparted.sourceforge.net/

Per allargare la partizione di root i passi da seguire sono:

- backup
- avviare la live: cancellare le partizioni di home e di swap, allargare la
  root e ricreare le partizioni di home e swap

A questo punto bisogna aggiornare i punti di mount, o meglio gli UUID (che
sono da preferire).  Da dentro la live, bisogna montare il device dell'hard
disk, quindi::

   blkid               # mostra gli id di tutte le partizioni
   blkid /dev/sda1     # mostra l'id di una sola partizione

Non resta che copiare i nuovi id in /etc/fstab e riavviare.

Risorse:
http://www.cyberciti.biz/faq/linux-finding-using-uuids-to-update-fstab/

Un altro problema di questa operazione è dato dalla mancanza di alcuni file
presenti nella propria home e necessari per l'avvio dell'interfaccia
grafica.  In questo caso è bene aprire un terminale con Ctrl + Alt + F1 e
copiare i dati di backup (attenzione ai permessi)::

   mkdir /home/fede
   chmod fede:fede /home/fede
   mount /dev/sdb1 /media/usb
   cp -r /media/usb/backup/fede/* /home/fede/



Filesystem
==========

ext4 è il filesystem più moderno, supportato a partire dal kernel 2.6.28.
Per i dischi a stato solido (SSD) e soprattutto per le penne con memoria flash
conviene disabilitare il journaling, che scrivendo spesso su disco aumenta il
consumo di queste penne.

Vediamo qual è il nome del device già formattato in ext4::

   fdisk -l


Questo comando ci permette di vedere se il journaling è attivato::

   dumpe2fs -h /dev/sdXN


Con questo comando si toglie::

   tune2fs -O ^has_journal /dev/sdXN


Fonte: http://www.oldwildweb.com/Ext4%20senza%20Journaling.htm

