******
Colore
******

Teoria sulla gestione del colore:

- http://www.colorwiki.com/wiki/The_Color_of_Toast
- http://www.argyllcms.com/doc/ColorManagement.html
- http://www.boscarol.com/blog/

Argyll
======

La calibrazione su Linux viene fatta da Argyll (programma a linea di comando)
anche se è più comodo usare delle interfacce grafiche, come DispcalGUI o Gome Color Manager,
per calibrare.  Se ci sono dei problemi con le GUI è bene verificare che
la calibrazione funzioni con argyll (da linea di comando).

Per installare l'ultima versione stabile di Argyll, probabilmente si dovrà
compilare.  ``apt-get build-dep argyll`` dovrebbe installare tutte le
dipendenze.  Poi si scaricano i sorgenti e si lanciano questi comandi::

    jam -f Jambase
    jam -f Jambase install

Gli eseguibili vengono salvati nella directory bin/ dei sorgenti.  Per
aggiungerli al PATH si possono copiare in ``~/bin/argyll/``.





Colorhug
========

Riferimenti:

- http://www.hughski.com/owner.html
- https://live.gnome.org/ColourManagement
- http://www.freedesktop.org/software/colord/index.html


Il client del colorimetro Colorhug è pacchettizzato per Debian.  Installiamo
tutti i pacchetti che ci serviranno::

    aptitude install colorhug-client argyll gnome-color-manager

.. NOTE::
   colorhug-client è solo un insieme di utility specifiche di ColorHug.
   argyll e gnome-color-manager sono invece i pacchetti necessari per
   fare la vera e propria calibrazione (teoricamente con qualsiasi dispositivo).

Se si usa il proprio sistema invece del CD live, occorre verificare che alcuni
pacchetti siano aggiornati alle versioni indicate nel sito di ColorHug::

   aptitude show argyll colord colorhug-client libgusb2 | grep Version:


Dopo aver connesso il colorhug si verifica subito il funzionamento con questo
comando, che dovrebbe far illuminare il led verde cinque volte::

   /usr/lib/colorhug-client/colorhug set-leds 1 5 200 200

Il seguente comando verifica se c'è una versione più recente del firmware e
nel caso la installa::

   colorhug-flash

Conviene scaricare le matrici di calibrazione contribuite dalla comunità,
per migliorare i risultati della calibrazione::

   colorhug-ccmx

Bisogna premere Refresh/Aggiorna per scaricare le matrici, ovvero file .ccmx
che vengono salvati in ``.local/share/colorhug-ccmx/``.  Quindi si può selezionare
la matrice del proprio monitor (se disponibile, altrimenti si possono tenere
le impostazioni predefinite del produttore).

Infine si lancia il programma di calibrazione, da Impostazioni»Colore oppure
da terminale con::

    gnome-control-center color

Si seleziona il dispositivo e si preme su Calibra.

I profili vengono salvati in ``~/.local/share/icc``.

In questa cartella si trova anche un profilo il cui nome inizia con edid-
È un profilo autogenerato da Gnome Color Manager ed è quello predefinito dello
schermo.

Riferimenti:

- http://dantti.wordpress.com/2012/03/14/self-color-calibration/
- http://en.wikipedia.org/wiki/Extended_display_identification_data
- http://blog.pcode.nl/2012/06/15/colorhug-red-shift-workaround/

Nelle impostazioni di Gnome, cliccando sui dettagli di ciascun profilo, si scopre
qual è il file .icc a cui si riferisce.




DispcalGUI
==========

DispcalGUI è un frontend grafico di argyll ed è pacchettizato in Debian::

    aptitude install dispcalgui


Se non fosse disponibile nel repository in uso si possono usare i
sorgenti.  Installiamo le dipendenze::

   aptitude install python-wxgtk2.8 python-numpy


Quindi si scarica il tarball dei sorgenti e si lancia l'eseguibile (non è
necessario installare!)::

   python dispcalGUI.pyw


Per installare si usa la procedura comune a tutti i pacchetti python
basati su setuptools::

    python setup.py build
    sudo python setup.py install

L'eseguibile sarà ``/usr/local/bin/dispcalGUI``.

Per verificare se il dispositivo di calibrazione è riconosciuto a livello di
sistema e da argyll::

   dmesg | tail
   dispcal -?


DispcalGUI, diversamente da colord, riconosce anche il ColorMunki Display e
non solo ColorHug.  Ha anche il vantaggio di capire subito se una calibrazione
andrà a buon fine, perché prima di avviarla testa il livello di bianco.

Ho notato che col ColorMunki i valori sono buoni, perché i valori di rosso,
verde e blu sono tutti vicini al centro.  Invece se uso ColorHug ho dei
valori molto sbilanciati.

Le opzioni da scegliere in DispcalGUI sono:

- scegliere il preset Laptop, che caricherà le curve di calibrazione presenti
  in `/usr/share/dispcalGUI/presets/laptop.icc`.
- lasciare le impostazioni predefinite, in particolare il livello di bianco e
  di nero nativi.
- avviare la misurazione del punto di bianco.  Se si tratta di un monitor
  esterno si potranno cambiare i valori di rosso, verde e nero in modo da
  ottenere il punto di bianco desiderato.  Se invece si sta calibrando il
  monitor di un laptop, bisogna sperare che i valori siano giusti, perché
  non è possibile cambiarli.
- non mi pare ci sia una grossa differenza tra qualità da Media a Alta.
- se si usano più strumenti di calibrazione è meglio che sia visibile nel
  nome del profilo: usare il wildcard `%in`.


Vista
=====

È interessante la possibilità di invertire i colori dello schermo, in modo che
ad esempio una pagina web con testo nero su fondo bianco diventi bianca su
fondo nero.  Non c'entra niente con i temi del desktop, è una modifica
relativa al monitor e può  essere comodo per qualche problema di vista.  Si
può usare questo comando per attivarla e disattivarla::

    xcalib -i -a

