Backup
======

Risorse:

- http://it.wikipedia.org/wiki/Rsync
- http://www.linuxguide.it/docs.php?Amministrazione_sistema:Backup_dati:Rsync
- http://shaytan.wordpress.com/2007/08/14/il-backup-incrementale-di-rsync/

Rsync è un potente strumento per la copia e la sincronizzazione di file
sia in locale che in remoto.

Il tipico comando di sincronizzazione è il seguente::

   rsync -av percorso/sorgente percorso/backup


.. WARNING::
   Fare attenzione all'ordine dei percorsi delle cartelle! PRIMA la cartella
   di cui fare il backup, POI il percorso alla cartella di backup.  Se succede
   il contrario sono guai!

Bisogna fare attenzione ai / finali (trailing slash).  Il / significa "copia
tutti i contenuti della directory", quindi se non c'è viene copiata la
directory nel percorso di destinazione.  In altre parole, i seguenti
comandi sono identici::

   rsync -av /src/foo /dest
   rsync -av /src/foo/ /dest/foo/

L'opzione -a è la modalità archivio, che conserva anche i link simbolici, i
permessi e i proprietari ed è ricorsiva; l'opzione -v serve invece a
stampare un output dettagliato.

Altre opzioni utili:

* -n (dry run mode), che non compie nessuna azione ma mostra quali sarebbero
  i risultati.  Si può appendere " | less" al comando per scorrere lentamente
  l'output.

* --delete: cancella eventuali file nella directory di backup che non sono (più)
  presenti nella directory principale

* --exclude="\*.xvd", per escludere file o directory che non ci interessano

* -z: per comprimere i dati di trasferimento (utile per i collegamenti remoti)

Nel caso di trasferimento di file verso un computer remoto, occorre specificare
l'utente, l'IP della macchina o l'URI del sito e il percorso completo di
destinazione::

   rsync -av /src/foo root@sito.it:/var/www

rsync si rivela molto utile anche per tenere sincronizzato un sito in locale con
la versione presente sul server.  Si può creare un file .sh eseguibile
all'interno del progetto con un comando simile:

.. code-block:: bash

    #!/bin/sh
    rsync -av --delete --exclude=dir-da-escludere * user@server.com:percorso/progetto/


