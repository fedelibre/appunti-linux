***********************
APT, gestione pacchetti
***********************

Conviene installare il pacchetto ``aptitude-doc-it`` per avere in locale la
documentazione italiana di aptitude: file:///usr/share/doc/aptitude/html/it/index.html


Repository
==========

La lista dei repository viene definita in ``/etc/apt/sources.list``.  Solitamente
si raccomanda l'uso di un mirror Debian.  Meglio ancora usare http://http.debian.net/,
che individua i mirror attivi e scarica da più mirror, aggirando così i problemi
temporanei che alcuni mirror possono avere.

Ecco un esempio del mio `sources.list`::

    # Debian Testing
    deb http://http.debian.net/debian/ testing main contrib non-free
    deb-src http://http.debian.net/debian/ testing main contrib non-free

    # Debian Unstable
    deb http://http.debian.net/debian/ unstable main

    # Debian Security
    deb http://security.debian.org/ testing/updates main contrib non-free
    deb-src http://security.debian.org/ testing/updates main contrib non-free


.. NOTE::
   Non bisogna usare i nomi delle release Debian (squeeze, wheezy, ...) se
   si vuole rimanere su una testing anche quando l'attuale testing diventa
   stabile.

Può essere utile creare un repository locale "fittizio" per far sì che i
pacchetti .deb scaricati dalla rete siano visibili a APT e quindi gestibili
come qualsiasi pacchetto.  Il principale vantaggio è che i .deb locali non
saranno  considerati obsoleti,  quindi si potranno disinstallare tutti i
pacchetti obsoleti con ``aptitude purge ?obsolete`` senza togliere anche
i pacchetti locali.  Le istruzioni sono descritte in:
https://wiki.debian.org/it/DebianRepository/HowTo/TrivialRepository

Uso questo sistema ad esempio per alcuni pacchetti .deb scaricati da
deb-multimedia.org, che preferisco non includere nel sources.list
perché creerebbe dei conflitti con i pacchetti ufficiali di Debian.



Pinning
=======

Nell'esempio precedente ho messo anche i repository di unstable, in modo da
poter usare versioni più recenti dei pacchetti quando ne ho bisogno.
Ma devo assicurarmi che i pacchetti di unstable non entrino in conflitto con
quelli di testing, attraverso il **pinning**.

Bisogna creare due file.  ``/etc/apt/preferences``::

    Package: *
    Pin: release a=testing
    Pin-Priority: 900

    Package: *
    Pin: release o=Debian
    Pin-Priority: -10

``/etc/apt/apt.conf``::

    APT::Default-Release "testing";
    APT::Cache-Limit 24000000;
    Apt::Get::Purge;
    APT::Clean-Installed;
    APT::Get::Fix-Broken;
    APT::Get::Fix-Missing;
    APT::Get::Show-Upgraded "true";


Quando avrò bisogno di installare un pacchetto di unstable::

    apt-get install -t unstable pacchetto

Riferimenti: http://guide.debianizzati.org/index.php/APT_uso_avanzato:_mixare_releases_diverse


Aggiornamento
=============

Ci sono due modi (il primo è raccomandato)::

	# aptitude safe-upgrade
	# aptitude full-upgrade


Il full-upgrade implica un tentativo di aggiornamento forzato, ovvero con rischio
rimozione pacchetti con dipendenze non soddisfatte.  Comunque impedisce
l'aggiornamento di un pacchetto se non ci sono dipendenze disponibili.

In questo caso, lanciare aptitude. Entrare nella sezione pacchetti aggiornabili,
selezionare il pacchetto incriminato e premere *d* per vedere le sue dipendenze.
Si noteranno dei pacchetti sottolineati in rosso, la versione di questi pacchetti
è inferiore a quella richiesta dal pacchetto bloccato.  La soluzione di solito
consiste nell'aspettare che i pacchetti vengano aggiornati.  Oppure, se presenti
in unstable, prenderli da lì col pinning.

Per sapere quali pacchetti sono stati aggiornati nell'ultimo safe-upgrade::

	less /var/log/dpkg.log



apt-listbugs
============

Uno strumento molto utile per evitare di aggiornare pacchetti che hanno dei
bug seri è apt-listbugs::

    aptitude install apt-listbugs

Per bloccare il pacchetto o i pacchetti segnalati si inserisce *p* e poi si
blocca l'aggiornamento.  Infatti apt-listbugs crea delle righe in ``/etc/apt/preferences``
che bloccano il pacchetto fino a quando il bug non sarà risolto, dunque aptitude
deve rileggere queste nuove impostazioni.



Ricerca
=======

La ricerca predefinita viene fatta nel nome del pacchetto, quindi i seguenti
comandi sono uguali::

    aptitude search python
    aptitude search ~npython

Per cercare nella descrizione::

    aptitude search ~deditor

Per cercare solo i pacchetti installati::

    aptitude search ~ipython


Talvolta si ha necessità di conoscere e eventualmente rimuovere i pacchetti
installati appartenenti a uno specifico repository Debian.  Il comando migliore
è questo (~Anomerepository)::

    aptitude search ~S~i~Aexperimental


Se invece vogliamo individuare tutti i pacchetti di una specifica origine,
ad esempio un repository non ufficiale, bisogna trovare l'origine indicata
da ``apt-cache policy``: è la stringa introdotta da o=.  Ad esempio, per
i repository di nginx::

     500 http://nginx.org/packages/debian/ squeeze/nginx amd64 Packages
     release v=6.0,o=nginx,a=stable,n=squeeze,l=nginx,c=nginx
     origin nginx.org


Quindi posso cercare (o rimuovere) tutti i pacchetti installati da questo repository::

    aptitude search ~i~Onginx
    aptitude remove ~i~Onginx
    aptitude search '~i (~O"Unofficial Multimedia Packages")'

