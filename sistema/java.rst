****
Java
****

Plugin Java
===========

I plugin java usati da tutti i browser installati nel sistema sono forniti
da questi pacchetti::

    aptitude install icedtea-6-plugin icedtea-7-plugin

Si può vedere qual è la versione predefinita, e eventualmente cambiarla, con::

    update-alternatives --config mozilla-javaplugin.so


Questa modifica ha effetti su tutti i browser, non solo su Firefox.

.. NOTE::
   Su Chrome può capitare che il plugin java non venga eseguito se non è
   aggiornato all'ultima versione (per motivi di sicurezza).  O si aggiorna
   oppure si aggiunge il flag ``--allow-outdated-plugins`` al comando.
   `Maggiori informazioni <https://support.google.com/chrome/bin/answer.py?hl=en&answer=1181003&p=ib_outdated_plugin>`_.

Test java per verificare il funzionamento del plugin:
http://java.com/en/download/testjava.jsp


Riferimenti:

- https://help.ubuntu.com/community/Java
- http://wiki.debian.org/Java
