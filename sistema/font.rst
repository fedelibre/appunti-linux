.. _Six Bars Jail: http://www.sixbarsjail.it/
.. _font Aberration: http://www.dafont.com/aberration.font

****
Font
****

Font disponibili
================

Per elencare i font disponibili sul proprio sistema GNU/Linux si lancia
il seguente comando::

   fc-list



Aggiungere un font
==================

Per aggiungere un nuovo font, lo si copia nella propria home, in `~/.fonts`
e poi lo si installa aprendolo col "Visualizzatore tipo di carattere".

Vediamo come installare il `font Aberration`_, con cui è stato realizzato il
logo del `Six Bars Jail`_.

Creare la directory `.fonts` nella propria home e salvarci il file.  Di solito
ogni font è composto da vari file, quindi io preferisco creare una nuova
directory per ogni font.  Si apre il file .ttf col Visualizzatore tipo
di carattere e si clicca su Installa.

Il font sarà elencato dal comando fc-list::

   fc-list | grep -i aberration

E sarà visibile in ogni applicazione grafica (Inkscape, Gimp, Scribus,
LibreOffice, etc.).


