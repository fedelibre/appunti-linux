.. _LifeSaver2: http://f-droid.org/repository/browse/?fdid=com.textuality.lifesaver2
.. _SMS Backup & Restore: https://play.google.com/store/apps/details?id=com.riteshsahu.SMSBackupRestore&hl=it
.. _bloatware: http://en.wikipedia.org/wiki/Software_bloat
.. _lista di clockworkmod: http://www.clockworkmod.com/rommanager
.. _Cyanogenmod: http://www.cyanogenmod.com/
.. _Radicale: http://radicale.org/
.. _adb: http://developer.android.com/tools/help/adb.html
.. _heimdall: http://glassechidna.com.au/heimdall/

*******
Android
*******

Ci sono molte buone ragioni per voler sostituire il software preinstallato
su un telefono Android con un firmware alternativo, come Cyanogenmod:

- togliere il `bloatware`_ e eventuali applicazioni spyware
- uscire dal "mondo Google" per avere il controllo della propria privacy
- usare *quasi* solo software libero
- sfruttare le funzionalità avanzate di Cyanogenmod
- aggiornare a una versione più recente di Android

Maggiori informazioni:

- http://freeyourandroid.org/
- http://www.gnu.org/philosophy/android-and-users-freedom.html



Backup
======

I dati più importanti che bisogna salvare sono:

- i contatti, che conviene esportare in un unico file ``.vcf`` che si
  reimporterà successivamente.
- gli sms tramite l'app `SMS Backup & Restore`_, non libera (ma
  `LifeSaver2`_ non è affidabile).  Da Android 4.4 l'app Mesaggi
  è in grado di fare backup e reimportazione.

Può convenire fare un backup dell'intero sistema originale, per poter
ritornare alle condizioni di fabbrica.  Prima di flashare quindi faremo
anche un backup con l'immagine recovery.



Installazione
=============

I software e i dettagli di installazione variano a seconda del device, anche
se la procedura a grandi linee è la stessa.

I file da scaricare sono:

- l'immagine recovery del ROM Manager Clockworkmod (vd `lista di clockworkmod`_)
- l'ultima versione stabile di `Cyanogenmod`_ per il proprio dispositivo
- [opzionale] il file .zip delle Gapps (applicazioni Google): http://wiki.cyanogenmod.org/w/Gapps

Vanno copiati nella root della scheda SD, anche quella esterna.

.. NOTE::
   Clockworkmod è in grado di caricare i file zip anche dalla scheda
   SD esterna, quindi non serve ingegnarsi per riuscire a copiare
   nella root.

.. WARNING::
   Clockworkmod deve essere della versione giusta richiesta dalla
   versione di Cyanogenmod.  Per aggiornarlo conviene installare
   sul telefono l'app ROM Manager.

La prima volta che si installa Clockworkmod bisogna usare `heimdall`_::

    heimdall flash --recovery recovery.img --no-reboot

Per gli aggiornamenti successivi è preferibile usare l'app ROM Manager.

Quindi si spenge il telefono e si riavvia in **recovery mod** con
Volume giù + Menu + Tasto accensione.  Parte Clockworkmod e si
seguono questi passi:

- backup dell'installazione corrente
- cancellazione dei dati (wipe data)
- installazione da sdcard sia di cyanogenmod che (eventualmente) delle gapps
- riavvio



Rubrica e calendari
===================

Non mi piace usare servizi cloud per salvare e sincronizzare i miei contatti
e le mie attività.  La questione privacy in questo caso va presa davvero
sul serio.

LDAP
----

Il sistema più semplice per gestire i contatti è configurare un
server LDAP:
http://wiki.debian.org/LDAP/OpenLDAPSetup
http://www.debian-administration.org/article/OpenLDAP_installation_on_Debian

Il pacchetto in Debian si chiama ``slapd``.


OwnCloud
--------




Radicale
--------

Vediamo come installare e configurare `Radicale`_, un server
CalDAV e CardDAV.

Sul proprio server si installano questi pacchetti dal PyPI::

    pip install radicale flup uwsgi

.. NOTE::
   flup e uwsgi servono solo se si vuole usare un server http come nginx.

Nel file di configurazione di nginx si aggiunge::

    server{
        listen 80;
        server_name cal.dominio.it;
        location / {
        include uwsgi_params;
        uwsgi_pass unix:///tmp/uwsgi.sock;
      }
    }

Nella directory del server (o dove si preferisce) si aggiunge il file .wsgi::

    import radicale
    radicale.log.start()
    application = radicale.Application()


Quindi si ferma nginx con ``service nginx stop`` e si esegue questo comando::

    uwsgi -s /tmp/uwsgi.sock -C --wsgi-file radicale.wsgi

.. TODO::
   devo imparare a lanciare questo comando in background

Si avvia radical come demone in background::

    radicale -d


Infine si riavvia nginx con ``service nginx start``.  Aprendo `cal.dominio.it`
si dovrebbe vedere una scritta tipo "Radicale works!".


Link utili:

- http://uwsgi-docs.readthedocs.org/en/latest/



Altre applicazioni
==================

Link utili:

- http://osmand.net/
- http://f-droid.org/
- http://gmtp.sourceforge.net/
