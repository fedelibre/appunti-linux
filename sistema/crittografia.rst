.. _GNU Privacy Guard: http://www.gnupg.org/

************
Crittografia
************

GPG
===

Link utili:

- `GNU Privacy Guard`_: programma a linea di comando
- `Seahorse <https://wiki.gnome.org/Apps/Seahorse>`_: frontend di GPG
  per il desktop Gnome.
- http://keyring.debian.org/creating-key.html
- https://wiki.debian.org/it/Keysigning
- https://we.riseup.net/riseuplabs+paow/openpgp-best-practices

Seahorse offre una comoda interfaccia per cercare e importare le
chiavi pubbliche, partendo dall'id della chiave.  Ma conviene
conoscere anche il relativo comando da shell::

  gpg --keyserver keys.gnupg.net --recv-key <id-chiave>



Client email
============

Geary
-----

Non supporta ancora la crittografia.  Tenere d'occhio il
`bug #713403 <https://bugzilla.gnome.org/show_bug.cgi?id=713403>`_ (GPG)
e il `bug 712895 <https://bugzilla.gnome.org/show_bug.cgi?id=712895>`_
(per includere l'id della chiave pubblica nella firma).


Thunderbird
-----------

Il plugin `Enigmail <http://www.enigmail.net/>`_ permette di:

- creare una coppia di chiavi
- cercare e importare le chiavi pubbliche dei propri contatti
- firmare e crittare i messaggi
- è sincronizzato con Seahorse
