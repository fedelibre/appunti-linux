.. _Debian Installer: http://www.debian.org/devel/debian-installer/

******
Debian
******

Installazione
=============

Riferimenti:

- http://www.debian.org/CD/faq/
- http://www.debian.org/releases/stable/installmanual
- http://www.debian.org/devel/debian-installer/
- http://guide.debianizzati.org/index.php/Installare_Debian_da_pendrive_USB_o_SD_card


#. si scarica il `Debian Installer`_ (DI), che contiene la versione
   testing (non esiste una versione per sid, bisogna fare l'upgrade
   successivamente).  Se si ha una connessione a Internet è meglio
   la versione netinst, altrimenti conviene usare il primo CD (che
   contiene il software più importante).
#. si formatta una chiave USB con un qualsiasi filesystem, aggiungendo
   il flag "bootable".
#. si scompatta il file .iso nella penna usb col comando
   ``cp nomedellimmagine.iso /dev/sdX`` (verificare il nome giusto del
   device con ``fdisk -l``).
#. si verifica che il BIOS del computer dia priorità al device USB al boot
#. si collega la penna e si seguono le istruzioni di installazione


Clonazione e ripristino sistema
===============================

Per clonare un'intera partizione o un intero disco, si può usare una
distro live (come `Kali <https://www.kali.org/>`_) e il programma ``dd``.
Questo comando crea l'immagine compressa dell'intero disco::

    dd if=/dev/hda conv=sync,noerror bs=64K | gzip -c  > /media/.../file.img.gz

Per ricopiarla::

    gunzip -c /media/.../file.img.gz | dd of=/dev/hda

Riferimenti: https://wiki.archlinux.org/index.php/disk_cloning


Pulizia dati
============

Per essere sicuri che i dati di una partizione non possano essere recuperati
dopo l'eliminazione (per esempio se si sta vendendo o regalando il proprio
computer) occorre usare un programma che vada a sovrascrivere i dati in modo
da offuscarli.  Si individua la partizione contenente la home e poi si
lancia il programma shred::

    mount | grep home
    shred -v -n 1 /dev/sXN

L'opzione -n limita la sovrascrittura a un passaggio, invece dei tre predefiniti.
