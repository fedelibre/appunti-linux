.. _Jack Audio Connection Kit: http://www.jackaudio.org/
.. _FFADO: http://www.ffado.org/
.. _lista delle schede supportate da FFADO: http://www.ffado.org/?q=devicesupport/list
.. _PulseAudio: http://www.pulseaudio.org/
.. _documentazione di PulseAudio: http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User
.. _Qjackctl: http://qjackctl.sourceforge.net/

*****
Audio
*****

Accesso alla scheda audio
=========================

L'accesso alla scheda audio varia da distribuzione a distribuzione.  Si può
scoprire quale sistema è in funziona sul proprio computer con questo comando::

    $ ls -l /dev/snd
    drwxr-xr-x  2 root root       60 nov 17 10:10 by-path
    crw-rw---T+ 1 root audio 116,  7 nov 17 10:10 controlC0
    crw-rw---T+ 1 root audio 116,  6 nov 17 10:10 hwC0D0
    crw-rw---T+ 1 root audio 116,  5 nov 17 10:10 hwC0D1
    crw-rw---T+ 1 root audio 116,  4 nov 17 18:56 pcmC0D0c
    crw-rw---T+ 1 root audio 116,  3 nov 17 18:56 pcmC0D0p
    crw-rw---T+ 1 root audio 116,  2 nov 17 10:10 pcmC0D2c
    crw-rw---T+ 1 root audio 116,  1 nov 17 10:10 seq
    crw-rw---T+ 1 root audio 116, 33 nov 17 10:10 timer

Il + finale indica che a gestire l'accesso è udev e PolicyKit, che dà accesso
alla scheda in modo dinamico all'utente che ne fa richiesta.  Tuttavia il
fatto che questi file appartengano al gruppo audio significa che è possibile
anche usare il vecchio sistema di aggiungere l'utente al gruppo audio.

Fonte: http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/PerfectSetup




Kernel realtime
===============

I kernel più recenti usano un nuovo stack firewire:

- https://help.ubuntu.com/community/FireWire#FireWire_stacks_explained
- https://ieee1394.wiki.kernel.org/index.php/Juju_Migration

Per fare delle registrazioni audio di qualità è davvero necessario usare
un kernel a bassa latenza (real time)?  La risposta breve è che probabilmente
non ce n'è bisogno:

- http://www.jackaudio.org/realtime_vs_realtime_kernel
- http://guide.debianizzati.org/index.php/Low-latency_2.6_kernel_per_applicazioni_audio_realtime
- http://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/Musicians_Guide/chap-Musicians_Guide-Real_Time_and_Low_Latency.html

Quello che invece bisogna fare per poter lavorare in realtime (anche con un
kernel normale) è abilitare il gruppo `audio` ai privilegi di realtime.
In ``/etc/security/limits.conf`` aggiungere alla fine del file le seguenti linee::

    @audio          -       nice            -10
    @audio          -       rtprio          99
    @audio          -       memlock         250000

Naturalmente il proprio utente deve far parte del gruppo audio.

.. NOTE::
   Il kernel real time è entrato nei repository a partire da Debian 7 (Wheezy).

Per evitare gli xrun potrebbe essere necessario questo comando (pacchetto
cpufrequtils)::

    sudo cpufreq-set -g performance


.. _jack:

JACK Audio Connection Kit
=========================

Installiamo i pacchetti necessari::

    aptitude install jackd2 jackd2-firewire libffado2 ffado-tools qjackctl multimedia-firewire


`Jack Audio Connection Kit`_ (JACK) è un server audio professionale, mentre
`FFADO`_ è un driver
per schede audio firewire (le migliori in termini di prestazioni e velocità nella
trasmissione dati).  Prima di comprare una scheda conviene controllare la
`lista delle schede supportate da FFADO`_.
Io ho una `ESI QuatafireXL <http://www.esi-audio.com/products/quatafirexl/>`_.

Jack può essere lanciato da terminale scegliendo il driver opportuno, ad
esempio::

    jackd -dalsa

Il realtime è già impostato di default.  L'opzione più interessante ai fini
della latenza è ``-p``.

La **latenza** è il tempo che passa tra il momento in cui il suono viene prodotto
e il momento in cui viene sentito.  Ma la bassa latenza è davvero utile solo
in certe situazioni, ad esempio quando si vuole ascoltare la propria voce
mentre si canta.  Questo articolo spiega molto bene l'argomento:
http://apps.linuxaudio.org/wiki/jack_latency_tests

Esiste anche un'interfaccia grafica, `Qjackctl`_, che permette di specificare
le opzioni senza dover usare la linea di comando e il manuale di jackd.  Ogni
volta che jack viene lanciato, viene creato/aggiornato il file ``~/.jackdrc``
che contiene il comando con tutte le opzioni di jackd: quindi è un ottimo
sistema per scoprire quali sono le opzioni più utili.

Ma la vera comodità di Qjackctl sono i preset, che permettono di salvare diversi
tipi di comandi.  Ad esempio, per creare un preset apposito per la scheda
firewire (diverso dal predefinito che usa Alsa), basta scrivere il nome nel
campo Preset, cambiare le impostazioni e salvare.

.. index:: ffado; firewire; kernel, firewire

Per testare un collegamento via firewire si può usare il comando::

    ffado-diag






Pulseaudio
==========

`Pulseaudio`_ è il server audio predefinito nelle moderne distribuzioni
GNU/Linux.  Per configurarlo bene conviene fare riferimento alla
`documentazione di PulseAudio`_.

Conflitto con Alsa
------------------

I programmi che inviano l'output direttamente a Alsa, a quanto ho
capito, saranno muti se PulseAudio è installato ed è il server audio
predefinito.  Bisogna dire ad Alsa di usare sempre PulseAudio come
driver, inserendo in ``~/.asoundrc`` le seguenti linee::

    pcm.!default {
        type pulse
    }

    ctl.!default {
        type pulse
    }

Riferimenti:
http://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/PerfectSetup


Conflitto con Jack
------------------

Pulseaudio e Jack non vanno molto d'accordo insieme:

- http://jackaudio.org/pulseaudio_and_jack
- http://trac.jackaudio.org/wiki/WalkThrough/User/PulseOnJack

Tra le soluzioni possibili ho scelto di reindirizzare PulseAudio su Jack
quando Jack è avviato.  Serve questo pacchetto::

    aptitude install pulseaudio-module-jack


E poi il file ``~/.pulse/default.pa`` con queste linee::

    load-module module-native-protocol-unix
    load-module module-jack-sink channels=2
    load-module module-jack-source channels=2
    load-module module-null-sink
    load-module module-stream-restore
    load-module module-rescue-streams
    load-module module-always-sink
    load-module module-suspend-on-idle
    set-default-sink jack_out
    set-default-source jack_in


Quindi si riavvia Pulseaudio (col proprio utente)::

    pulseaudio -k
    pulseaudio -D

In Impostazioni»Audio, nella scheda Uscita, si vedrà **Jack sink**.
Questo mi permette ad esempio di usare Stretchplayer (che usa Jack)
insieme a Vlc o a un browser (che usano invece PulseAudio), senza
dover cambiare configurazione del server audio.


MIDI
====

Per riprodurre i file MIDI conviene usare `fluidsynth <http://www.fluidsynth.org/>`_::

    aptitude install fluidsynth

Test da linea di comando::

    fluidsynth -a alsa /usr/share/sounds/sf2/FluidR3_GM.sf2 file.midi

.. NOTE::
   Di default fluidsynth usa JACK come driver, per questo specifico ALSA.


Si può impostare fluidsynth come servizio da avviare a ogni sessione
dell'ambiente desktop seguendo la
`specifica autostart di Freedesktop <http://standards.freedesktop.org/autostart-spec/autostart-spec-latest.html>`_

Si crea il file ``~/.config/autostart/fluidsynth.desktop`` con un
contenuto simile::

    [Desktop Entry]
    Comment[en_US]=FluidSynth MIDI Synthesizer
    Comment=FluidSynth MIDI Synthesizer
    Encoding=UTF-8
    Exec=fluidsynth -a pulseaudio -m alsa_seq -i -s -p FluidSynth -R 0 -C 0 -c 2 -z 512 -r 48000  /usr/share/sounds/sf2/FluidR3_GM.sf2
    GenericName[it]=Sintetizzatore MIDI
    GenericName=MIDI Synth
    Icon=system-run
    MimeType=
    Name[en_US]=FluidSynth
    Name=FluidSynth
    Path=
    StartupNotify=true
    Terminal=false
    TerminalOptions=
    Type=Application

Se fluidsynth è attivo come servizio, si può usare anche pmidi per riprodurre
un file MIDI da linea di comando.  È necessario specificare la porta::

    pmidi -l
    pmidi -p "128:0" file.midi

