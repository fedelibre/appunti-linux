*****************
Macchine virtuali
*****************

La virtualizzazione è uno strumento molto utile per testare altri
sistemi operativi o altre distribuzioni Linux.


VirtualBox
==========

La versione open source è presente nei repository::

    aptitude install virtualbox


Avvia VirtualBox, premi il pulsante Nuova, scegli il sistema
operativo e segui la procedura di configurazione della macchina
virtuale.

Per lanciare la macchina si usa il comando (conviene salvarlo in
un file .desktop)::

    VBoxManage startvm <nome-macchina>

Talvolta è necessario allargare lo spazio della macchina virtuale::

    vboxmanage list hdds                        # mostra l'UUID dei dischi
    vboxmanage modifyhd <UUID> --resize 10240   # aumenta fino a 10GB di spazio


Cartelle condivise
------------------

Perché le cartelle condivise possano funzionare, il sistema guest (se è linux)
deve avere installato dkms e gli header del kernel::

    apt-get install dkms linux-headers-$(uname -r|sed 's,[^-]*-[^-]*-,,')

La cartella condivisa si definisce nelle impostazioni di ciascuna macchina
virtuale.  Per richiamarla si usa il nome (non il percorso dell'host, che non
ha senso dentro il guest)::

    mount -t vboxsf [-o OPTIONS] sharename mountpoint

Riferimenti:

- https://www.virtualbox.org/manual/
- https://wiki.debian.org/VirtualBox



libvirt
=======

Installazione
-------------

Si installano i pacchetti più utili per lavorare con libvirt e si
dà al proprio utente il permesso di gestire le macchine virtuali::

    aptitude install virt-manager virt-viewer libguestfs-tools
    adduser <utente> libvirt    # uscire dalla sessione e rientrare

Per installare Windows da CD bisogna prima avviare la rete `default`
e poi lanciare ``virt-install``::

    sudo virsh net-start default
    virt-install --name=Windows --os-type=windows \
    --network network=default \
    --disk path=/path/to/file.img,size=15 \
    --cdrom=/dev/sr0 --graphics spice --ram=1024

L'unità di misura di ``--disk size`` sono i GB, mentre quello
di ``--ram`` sono i MB.

Si possono installare anche delle distribuzioni GNU/Linux.  L'elenco
delle opzioni disponibili è dato da questo comando::

    virt-install --os-variant list

Ecco un esempio di come installare Fedora 20::

    virt-install --name fedora20 --ram 1024 \
    --os-variant fedora20 --disk path=/percorso/al/file.img,size=10 \
    --location http://dl.fedoraproject.org/pub/fedora/linux/releases/test/20-Alpha/Fedora/x86_64/os

Oppure si può usare ``virt-builder``, ancora più semplice::

    virt-builder --list
    virt-builder fedora-20

Anche se ho dovuto usare un comando più complesso per evitare l'errore
di resizing del disco::

    virt-builder fedora-20 --format qcow2 --size 8G
    kvm fedora-20.qcow2


Gestione
--------

La macchina virtuale così creata può essere avviata sia con
virt-manager che con Boxes (Gnome), due interfacce grafiche
di libvirt.

Per vedere l'elenco delle macchine installate (e il loro status)::

    virsh list        # solo quelle attive
    virsh list --all  # tutte

Per testare la macchina la prima volta si possono usare
questi comandi::

    virsh net-start default
    virsh start <macchina>
    virt-viewer <macchina>

Per evitare di dover avviare la rete ogni volta, si può rendere
automatico l'avvio con::

    virsh net-autostart default

Quindi si può usare virt-manager per avviare e visualizzare la
macchina virtuale.

Per allargare la dimensione del disco di una macchina virtuale,
bisogna prima individuare la partizione da estendere (altrimenti
ne crea una nuova)::

    virt-filesystems --long --parts --blkdevs -h -a /percorso/al/disco-iniziale.img

Quindi si può estendere la partizione (di solito sda2, mentre sda1
è per il boot).  Ad esempio, per raddoppiare un disco da 10 a 20 GB::

    truncate -s 20G disco-espanso.img
    virt-resize --expand /dev/sda2 disco-iniziale.img disco-espanso.img

Il primo comando crea il nuovo file vuoto, mentre il secondo copia
il vecchio file nel nuovo file di maggiori dimensioni.

Riferimenti: http://libguestfs.org/virt-resize.1.html



Boxes
=====

È pacchettizzato come ``gnome-boxes`` (si consiglia versione >=3.8).

Per abilitare il copia e incolla da guest a host (e viceversa), il
ridimensionamento automatico dello schermo del guest e il drag and drop
dei file (solo da host a guest) serve `spice-vdagent <http://spice-space.org/>`_.
Su Linux si installa il pacchetto ``spice-vdagent``, mentre su Windows
si installa il programma ``spice-guest-tools``.

gnome-boxes permette di montare i dispositivi usb. Se si montano sul guest
vengono smontati dall'host e viceversa.

Per avere una cartella condivisa tra guest e host conviene installare
Samba su Linux.  In breve, si tratta di definire in linux (host)
la directory di condivisione, aggiungendo queste righe a ``/etc/samba/smb.conf``::


    [pubblica]
        path = /home/nome/directory
        public = yes
        writable = yes

In Windows si apre Computer e si clicca su `Connetti unità di rete`.
La scelta dell'unità è indifferente, mentre la cartella deve essere
composta dall'IP dell'host + il nome della directory condivisa definita
nel file di configurazione di Samba.  Ad esempio ``\\10.0.20.10\pubblica``.

Non ho ancora ben chiaro come funzionano i permessi sulla durectory
condivisa, ma in caso di errore basta dare permesso di scrittura
a tutti (777).

Boxes si appoggia a libvirt, dunque si possono usare i suoi tool
per fare delle modifiche che non sono accessibili tramite
l'interfaccia grafica di Boxes.  Boxes vede le macchine virtuali
elencate da questo comando (eseguito da utente normale):

    virsh -c qemu:///session list --all

Per fare delle modifiche si lancia questo comando::

    virsh -c qemu:///session edit <dominio>

Per esempio, se cambio il nome dell'immagine nel filesystem in
``~/.local/share/gnome-boxes/images`` dovrò aggiornare il
percorso nel file XML della macchina, tramite il comando
precedente.  Rinominare una macchina invece richiede qualche
passaggio in più::

    virsh dumpxml <nome-macchina> > file.xml
    virsh undefine <nome-macchina>
    ## poi si modifica il file.xml
    virsh define file.xml
    virsh start <nuovo-nome-macchina>
