********
Hardware
********

Per ottenere informazioni sull'hardware di un computer si usano i comandi
lspci, lsusb e lshw.  È  possibile ampliare le informazioni di lspci relative
a un device con l'opzione -v::

    $ lspci | grep VGA
    00:02.0 VGA compatible controller: Intel Corporation 3rd Gen Core processor Graphics Controller (rev 09)
    $ lspci -v -s 00:02.0
    00:02.0 VGA compatible controller: Intel Corporation 3rd Gen Core processor Graphics Controller (rev 09) (prog-if 00 [VGA controller])
	    Subsystem: Dell Device 058b
	    Flags: bus master, fast devsel, latency 0, IRQ 44
	    Memory at d0000000 (64-bit, non-prefetchable) [size=4M]
	    Memory at c0000000 (64-bit, prefetchable) [size=256M]
	    I/O ports at 2000 [size=64]
	    Expansion ROM at <unassigned> [disabled]
	    Capabilities: <access denied>
	    Kernel driver in use: i915


Un altro modo è lshw, magari filtrando per classe::

    sudo lshw -c video
    sudo lshw -c sound

Per avere maggiori informazioni sul driver in uso::

    sudo modinfo i915
