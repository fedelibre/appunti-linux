.. _Debian kernel handbook: http://kernel-handbook.alioth.debian.org/
.. _in locale: file:///usr/share/doc/debian-kernel-handbook/kernel-handbook.html/index.html

************
Kernel Linux
************

Documentazione:

- `Debian kernel handbook`_ (si può leggere `in locale`_ se si installa il
  pacchetto ``debian-kernel-handbook``)
- http://guide.debianizzati.org/index.php/Debian_Kernel_Howto (la guida migliore e più semplice)
- http://e-zine.debianizzati.org/web-zine/numero_1/?page=90


Dipendenze
==========

Pacchetti consigliati per compilare il kernel::

    apt-get install module-init-tools kernel-package libncurses5-dev fakeroot


Sorgenti
========

Esistono due versioni dei sorgenti:

#. i sorgenti *patchati* e distribuiti da Debian che si trovano nei pacchetti
   ``linux-source``. Hanno il pregio di essere adattati a Debian e in
   particolare non contengono gli elementi non liberi del kernel (spostati
   in ``firmware-linux-nonfree``); ma esistono solo le versioni distribuite
   da Debian.
#. i sorgenti vanilla distribuiti su https://www.kernel.org/ invece non sono
   stati ripuliti ma si può prendere qualsiasi versione.

.. NOTE::
   In ogni caso si consiglia di compilare come utente normale e non come root.
   Il kernel vanilla lo compilo nella home, mentre il kernel Debian, per
   convenzione, si compila in ``/usr/src`` e occorre aggiungere il proprio
   utente al gruppo ``src`` per poter scrivere in quella directory.


Kernel Debian
=============

Vediamo come riconfigurare e aggiungere delle patch al kernel Debian.
Si installano i sorgenti del kernel::

    aptitude install linux-source-3.2


I sorgenti vengono salvati in ``/usr/src``.  Dato che il kernel non deve
essere compilato da root, occorre fare in modo che l'utente normale
possa scrivere in questa directory::

    chown -R root:src /usr/src
    chmod -R g+w /usr/src
    adduser fede src

Occorre chiudere la sessione e rientrare per rendere effettiva l'aggiunta
al gruppo src.

Prepariamo i sorgenti::

    cd /usr/src
    tar xvjf linux-source-3.2.tar.bz2
    cd linux-source-3.2.tar.bz2
    make-kpkg clean  # questo comando non è necessario alla prima compilazione
    cp /boot/config-3.2.0-4-amd64 .config

Ora andiamo a scaricare e a applicare delle patch (in questo caso sono le
patch per il trackpad Cypressus, il cui supporto è stato introdotto
in versioni successive del kernel).  Ho scaricato le otto patch da
`qui <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=703607>`_.
Le applico una a una col comando ``patch -p1 -i file.patch``.


Ora possiamo compilare il kernel::

    make oldconfig  # per usare le stesse configurazioni del kernel in uso e vedere solo le nuove opzioni
    make menuconfig  # se si vuole togliere qualcosa che non ci serve
    make clean  # non serve alla prima compilazione
    fakeroot make deb-pkg
    cd ..
    sudo dpkg -i linux-image-3.2.41_3.2.41-1_amd64.deb





Kernel vanilla
==============

Vediamo come compilare un kernel vanilla, cosa inevitabile se si ha bisogno
di funzionalità abilitate solo in un kernel recente::

    mkdir ~/linux
    cd ~/linux
    wget https://www.kernel.org/pub/linux/kernel/v3.x/testing/linux-3.4.1.tar.xz
    unxz linux-3.4.1.tar.xz
    tar -xf linux-3.4.1.tar
    cd linux-3.4.1/
    fakeroot make deb-pkg
    cd ..
    sudo dpkg -i linux-XXXX.deb
