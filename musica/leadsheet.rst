.. _Chordii: http://www.vromans.org/johan/projects/Chordii/index.html

*********
Leadsheet
*********

Chordii
=======

`Chordii`_ è un semplice programma per generare leadsheet; è presente
nei repository Debian.

Per visualizzare gli accordi conosciuti da chordii si usa questo comando::

    chordii -D > chordii-chart.ps

Per visualizzare le definizioni di tali accordi::

    chordii -d

(in questo caso non è necessario redirigere l'output, si può leggere sul terminale)

Se l'accordo desiderato non è presente oppure si vuole modificare la diteggiatura
di un accordo, bisogna usare la direttiva ``define``, ad esempio::

    {define: F7maj4 base-fret 1 frets 0 3 3 2 0 0}


Gli accordi personalizzati possono essere salvati, insieme ad altre opzioni,
nel file ``~/.chordiirc``, in modo da non doverli ridefinire in ogni file.

.. WARNING::
   Purtroppo gli accordi vengono stampati alla fine del brano e non è possibile
   spostarli all'inizio per un difetto strutturale del programma (come mi
   ha spiegato l'attuale sviluppatore, che sta lavorando a una nuova versione
   in Perl).



LaTeX
=====

I leadsheet si possono creare anche con LaTeX, tramite i pacchetti CTAN
inclusi in questo pacchetto::

    aptitude install texlive-music

Il pacchetto migliore è `Songs <http://songs.sourceforge.net/>`_.  La documentazione
è disponibile online:
http://songs.sourceforge.net/songsdoc/songs.html

Per scrivere canzoni con caratteri accentati serve questa riga nel premabolo::

    \usepackage[utf8x]{inputenc}

Esiste un progetto basato su Songs e LilyPond:
http://www.patacrep.com/

