.. _Ardour: http://ardour.org/
.. _Qjackctl: http://qjackctl.sourceforge.net/
.. _alimentazione phantom: http://it.wikipedia.org/wiki/Alimentazione_phantom
.. _compilare Ardour: http://ardour.org/building

******
Ardour
******

`Ardour`_ è un Digital Audio Workstation professionale disponibile per Linux e Mac.

.. NOTE::
   Leggi prima :doc:`/sistema/audio`.


Comunità di utenti:

- http://linux-audio.com/


Tutorial su Ardour:

- http://en.flossmanuals.net/ardour/ (guida per iniziare)
- http://manual.ardour.org/
- http://vm-nice.stackingdwarves.net:8888/ardour-en/1-ARDOUR.html (manuale della community)
- http://arnout.engelen.eu/files/dev/linuxmusicians/ardourtutorial.html  (ottimo, semplice e chiaro)
- http://briansbedroom.blogspot.com/search/label/ardour (un blog molto specifico e per utenti avanzati)
- http://www.linuxjournal.com/node/1000224 (un articolo per utenti avanzati)

Manuali completi:

- http://carlh.net/ardour/ (Ardour 3)

Ci sono anche dei video tutorial elencati qui: http://ardour.org/support


Installazione
=============

Ardour è pacchettizzato per le varie distro Linux.  Ma può essere
utile saper `compilare Ardour`_ per testare le versioni più
recenti, come la 3.  Installiamo le dipendenze (da root)::

    apt-get build-dep ardour
    aptitude install libcppunit-dev uuid-dev


Quindi scarichiamo e compiliamo i sorgenti::

    git clone git://git.ardour.org/ardour/ardour.git
    cd ardour
    ./waf configure
    ./waf

Si può lanciare l'eseguibile ``./gtk2_ardour/ardev`` oppure installare
il programma con ``./waf install`` (percorso predefinito: `/usr/local/`)
e l'eseguibile è ``ardour3``.

Per creare l'icona d'avvio in Gnome 3::

    cp /usr/share/applications/ardour.desktop ~/.local/share/applications/ardour3.desktop

Quindi si modificano queste linee::

    Exec=/usr/local/bin/ardour3
    Icon=/usr/local/share/ardour3/icons/ardour_icon_48px.png


Jack
====

Ardour ha bisogno di Jack per funzionare.  Quindi se jack non è già stato
lanciato, Ardour propone una serie di configurazioni che permettono di
lanciare jack.  Altrimenti comparirà subito la finestra di salvataggio
della sessione di lavoro.

.. NOTE::
   È consigliabile registrare su un disco diverso da quello in cui si trova
   il programma, perché ovviamente quest'ultimo sarà continuamente occupato
   dalle richieste del programma.

Il primo problema che si può incontrare è proprio il funzionamento di Jack.
Vedi :ref:`jack`.



Hardware
========

Se si ha un microfono a condensatore, è necessario abilitare
l'`alimentazione phantom`_ della scheda audio cliccando sul
pulsante `+48v`.

Il pulsante Line/Mic a cosa serve?



Registrazione
=============

Conviene che il monitoring, ovvero l'ascolto di ciò che si registra, sia
gestito da Ardour.  In questo modo, quando si "arma" la traccia, ovvero la
si preparara per la registrazione, cliccando sul pallino rosso della traccia,
se si suona si dovrebbe vedere il segnale nei livelli della traccia.
Abilitando la vista del Mixer sulla sinistra si ha una visione più chiara
dei volumi e della distanza dal rischio di click.

Quindi si preme prima il bottone rosso di registrazione presente nella timeline
e poi il play.  Puoi iniziare a suonare.

.. NOTE::
   Se si vuole registrare da due fonti sonore, conviene attaccarle prima
   di lanciare Ardour, così che i collegamenti tra input e traccia siano
   individuati automaticamente.


Plugin
======

I plugin consigliati sono `LV2 <http://lv2plug.in/>`_::

    aptitude install lv2core


- http://manual.ardour.org/working-with-plugins/

