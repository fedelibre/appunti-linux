.. _Tuxguitar: http://sourceforge.net/p/tuxguitar/

******************
LilyPond front-end
******************

Ho iniziato a sperimentare alcuni front-end di LilyPond per vedere se
è possibile insegnare LilyPond cominciando da un'interfaccia grafica,
in modo simile ai corsi Dreamweaver+HTML di alcuni anni fa.


Denemo
======

`Denemo <http://denemo.org/>`_ è presente nei repository, ma spesso non è
aggiornato.  Per provare l'ultima versione si installano le dipendenze
indicate nel sito o, meglio ancora, suggerite dal ./configure::

    aptitude install libevince-dev libgtksourceview-3.0-dev libjack-jackd2-dev \\
    libfluidsynth-dev portaudio19-dev libportmidi-dev

.. WARNING::
   Per installare libfluidsynth-dev senza che vengano disinstallati jack2,
   ardour e altri programmi che mi servono, è necessario specificare libjack-jackd2-dev
   al posto di libjackd-dev.


Per compilare i sorgenti::

    git clone git://git.savannah.gnu.org/denemo.git
    cd denemo
    sh autogen.sh
    ./configure
    make
    make install

Il manuale si trova qui: file:///usr/local/share/denemo/manual/denemo-manual.html

L'uscita Midi deve essere correttamente impostata nelle Preferenze:

- Audio Backend: Portaudio
- Output device: Timidity (soluzione più semplice), oppure Jack

F5 per il play, F6 per lo stop.



Laborejo
========

`Laborejo <http://www.laborejo.org/>`_ un programma scritto in Python3 e
richiede queste dipendenze::

    aptitude install python3-dev python3-pyqt4 python3-pyqt4.qtopengl


Perché la riproduzione Midi funzioni, bisogna compilare con python3 questa
libreria (non ancora pacchettizzata in Debian)::

    wget http://das.nasophon.de/download/pysmf-0.1.0.tar.gz
    tar xzf pysmf-0.1.0.tar.gz
    cd pysmf-0.1.0
    python3 setup.py build
    sudo python3 setup.py install

Prima si lancia Jack (con Qjackctl) e poi Laborejo::

    ./laborejo-qt.sh



Schikkers List
==============

`Schikkers List <http://lilypond.org/schikkers-list/>`_ è sviluppato da uno
dei due principali autori di LilyPond.  Al momento è un prototipo, non molto
utilizzabile.

Istruzioni su come installare:
https://github.com/janneke/schikkers-list/blob/master/INSTALL



Tuxguitar
=========

`Tuxguitar`_ non è un frontend, ma può esportare in vari formati che si
possono poi usare in LilyPond:

#. lilypond: distingue le voci, ma non le legature di portamento o il
   glissando.  Il principale limite è che il codice non è molto pulito,
   quindi non è semplice modificarlo.
#. musicXML» musicxml2ly: non vengono distinte le voci, quindi è
   sconsiglabile per i brani polifonici.
#. MIDI» midi2ly: sconsigliato

Esportare direttamente in formato lilypond da Tuxguitar è la soluzione
migliore, anche se ci sarebbe molto da aggiornare e correggere.

La versione mantenuta su Sourceforge ha dei miglioramenti che non sono
stati ancora rilasciati in una nuova versione.  Per compilarlo occorre
installare le dipendenze e maven (il compilatore java)::

    apt-get build-dep tuxguitar
    apt-get install maven

Quindi si scaricano i sorgenti e si compila::

    svn checkout svn://svn.code.sf.net/p/tuxguitar/code/trunk tuxguitar
    cd tuxguitar/build-scripts/tuxguitar-linux-x86_64
    mvn clean package -Dnative-modules=true

Si lancia con::

    sh target/tuxguitar-1.3-SNAPSHOT-linux-x86_64/tuxguitar.sh
