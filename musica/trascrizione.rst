.. _stretchplayer: http://www.teuton.org/~gabriel/stretchplayer/
.. _clam-chordata: http://clam-project.org/
.. _transcribe: http://www.seventhstring.com/xscribe/

************
Trascrizioni
************

Software utili per le trascrizioni:

* `stretchplayer`_
* `transcribe`_ (non libero, ma gira anche su linux; ha un comodissimo EQ)
* `clam-chordata`_ (da usare con Jack)
* http://code.soundsoftware.ac.uk/projects/nnls-chroma
* http://chordify.net/
