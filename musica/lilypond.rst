.. highlight:: none

********
LilyPond
********

Questi appunti si riferiscono alla sintassi della versione 2.16.

Accordatura
===========

L'accordatura predefinita per la chitarra è ovviamente quella standard,
ovvero EBGDAE.  Le accordature per chitarra e altri strumenti si trovano
in ``ly/string-tuning-init.ly`` e sono elencate in un'appendice
della documentazione.  Per impostare queste accordature già definite::

    \set TabStaff.stringTunings = #guitar-open-g-tuning
    \set TabStaff.stringTunings = #guitar-open-d-tuning
    \set TabStaff.stringTunings = #guitar-dadgad-tuning
    ...

Si possono definire accordature non presenti in string-tuning-init.ly inserendo
le note in **modalità assoluta**::

    \set TabStaff.stringTunings = \stringTuning <d, bes, d g bes d'>


.. NOTE::
   Se l'accordatura definita non funziona come previsto probabilmente hai
   sbagliato a scegliere le note in modalità assoluta.  Per individuare
   subito l'errore conviene inserire le note della musica in modalità assoluta
   invece che relativa.


Armonici
========

Gli armonici in tablatura possono essere indicati con le loro altezze reali
e il comando \\harmonic::

    d'\4\harmonic g'\3\harmonic b'\2\harmonic e''\harmonic

Questo è possibile solo per quegli armonici in cui la nota prodotta è la
stessa della corda, ovvero per gli armonici al dodicesimo tasto.

Per gli altri armonici si usano \\harmonicByFret e \\harmonicByRatio.

\\harmonicByFret richiede il tasto su cui appoggiare il dito e l'altezza
della corda da usare::

    \harmonicByFret #7 a,2\5

In questo esempio schiaccio il settimo tasto della quinta corda (La, accordatura
standard).  Nella tablatura vedrò un <7> e nel rigo la nota Mi nel quarto spazio,
calcolata da LilyPond.

\\harmonicByRatio richiede invece il rapporto della lunghezza della corda
rispetto al tasto schiacciato::

    \harmonicByRatio #1/2 <g\3 b\2 e'\1>4
    \harmonicByRatio #1/3 <g\3 b\2 e'\1>4
    \harmonicByRatio #1/4 { g8\3 b8\2 e'4\1 }

Ovvero:

- 1/2 = dodicesimo tasto
- 1/3 = settimo e diciannovesimo tasto
- 1/4 = quinto e diciassettesimo tasto

Si veda la tabella di Wikipedia sugli
`armonici su strumenti a corda <http://en.wikipedia.org/wiki/Harmonic#Harmonics_on_stringed_instruments>`_

Infine per gli armonici artificiali, ovvero gli armonici prodotti schiacciando
un tasto e cambiando quindi la lunghezza della corda e la posizione degli
armonici sulla tastiera, si veda questo snippet:
http://lsr.dsi.unimi.it/LSR/Item?id=811



Diteggiature
============

Barré
-----

Per indicare un barré che si estende per più di una nota si usa il TextSpanner::

   \override TextSpanner #'(bound-details left text) = #"3/6 B XII "
   g16\startTextSpan
   b16 e g e b g\stopTextSpan


Ovviamente, se in battute successive c'è un nuovo barré sullo stesso tasto,
non c'è bisogno di ripetere il comando \\override: è sufficiente reinserire
\\startTextSpan e \\stopTextSpan.

Se il brano è composto da vari cambi di barré, l'input diventerà un
po' confuso.  Meglio allora usare questa funzione::

    barre =
    #(define-music-function (parser location strg)(string?)
    (let ((arg (string-append "B " strg)))
    #{
      \override TextSpanner #'(bound-details left text) = $arg
    #}))


Basta specificare il tasto dopo il comando::

    \barre II g\startTextSpan a b c\stopTextSpan
    \barre V c\startTextSpan d e f\stopTextSpan


Oppure si possono sostituire gli estremi del TextSpanner con delle parentesi
graffe, riducendo lo spazio occupato sulla linea::

    barre =
    #(define-music-function (parser location strg music)(string? ly:music?)
       (let ((arg (string-append "B " strg)))
          #{
             \override TextSpanner #'(bound-details left text) = $arg

             #(let ((elts (ly:music-property music 'elements)))
                  (make-music 'SequentialMusic 'elements
                     (append
                        (list (make-music 'TextSpanEvent 'span-direction -1))
                        (reverse (cdr (reverse elts)))
                        (list (make-music 'TextSpanEvent 'span-direction 1))
                        (list (last elts)))))
          #}))

Con questa funzione il comando diventa::

    \barre II { g a b c }


Se il barrè è su una nota soltanto, non possiamo usare uno spanner, che ha
bisogno di una nota di partenza e di una di arrivo.  Si userà allora il
normale markup::

   cis8^\markup {
     \null % è necessario come "anchor point" per spostare il testo successivo
     \raise #2 \italic { B II }
   }

Per indicare un "semibarré" che non si estende nella battuta si usa spesso
una parentesi quadra (visibile solo nel rigo)::

   \override TabVoice.Arpeggio #'transparent = ##t
   \arpeggioBracket
   <cis-1 fis>\arpeggio


Mano sinistra
-------------

Le indicazioni della mano sinistra sono semplicemente del testo che viene
attaccato alla nota, ovvero non c'è un controllo sintattico (potrei scrivere
anche 22, anche se ovviamente non esiste il ventiduesimo dito :-))::

    c4-1 d-3

Per controllare il posizionamento del numero occorre avvolgere la nota
dentro <>::

    \set fingeringOrientations = #'(left)
    <c-1>4 <g-2\4 c-3\3 e-4\2 g-1>2


Il pollice non è compreso nei fingering di Lilypond (o meglio, probabilmente
non è pensato per la chitarra, semmai per il pianoforte, per cui le dita vanno
da 1 a 5..come è naturale che sia).  Invece nel mondo della chitarra si
comincia a contare dall'indice (1) fino al mignolo (4). E il pollice è T.
Come renderlo su Lilypond? Usando il markup::

    g2_"T"
    g2_\markup { \finger "T" }

Il primo stampa una T con un tipo di carattere un po' "antico".
Il secondo invece stampa un carattere con una T più moderna, ma anche più fredda.


Mano destra
-----------

La diteggiatura della mano destra ha un comando apposito, \\rightHandFinger::

    c4-\rightHandFinger #1

Per semplificare l'input si può definire (all'inizio del file o in un file
esterno con le personalizzazioni) una scorciatoia per \\rightHandFinger::

    #(define RH rightHandFinger)

Vediamo un esempio in cui è presente anche la diteggiatura della mano sinistra:

    c-2-\RH #3


Gli accordi sono la situazione in cui la scorciatoia RH si dimostra molto utile::

    <c-\RH #1 e-\RH #2 g-\RH #3 c-\RH #4 >2


Anche in questo caso i segni p-i-m-a possono essere disposti dove si preferisce,
a patto che l'altezza sia racchiusa tra <>::

    \set strokeFingerOrientations = #'(up)
    <c-\rightHandFinger #1 >4

.. NOTE::
   Ci deve essere uno spazio prima del segno > che chiude l'accordo.


Per evitare questo warning::

    warning: Ignoring grob for slur: StrokeFinger. avoid-slur not set?

occorre far precedere il comando da un \\override::

    \override StrokeFinger #'avoid-slur = #'inside
    \set strokeFingerOrientations = #'(up)





Testo
=====

Indicazioni
-----------

Mi riferisco a tutte quelle indicazioni che spiegano come si succedono le
parti del brano: D.C., ripeti dal segno al segno, vai al Coda, etc.

Alcuni esempi::

    c4 c2\segno d4 |
    c1 \mark \markup { \musicglyph #"scripts.segno" } |
    d1

Il primo esempio crea un segno attaccato alla nota e quindi più piccolo.
Il secondo invece colloca il segno alla fine della battuta in cui si trova
e ha una dimensione più grande (lo preferisco).

Se si trova alla fine di un rigo bisogna cambiare l'allineamento::

    \once \override Score.RehearsalMark #'break-visibility = #end-of-line-visible
    \once \override Score.RehearsalMark #'self-alignment-X = #RIGHT
    \mark \markup { \musicglyph #"scripts.coda" }

I font Feta di solito devono essere allineati correttamente rispetto al testo
normale col comando \\translate::

    c1^\markup {
      D.C. al \translate #'(1.2 . 1) \musicglyph #"scripts.segno"
      \hspace #.5 poi segue
    }


Note a pié di pagina
--------------------

Bisogna usare i tag per evitare che la nota compaia sia in Staff che in TabStaff::

  \tag #'foot {
    \footnote "Nota 1" #'(-1.5 . 3)
    \markup \wordwrap {
      \justify {
        1. La percussione all'inizio della pausa si ottiene
        appoggiando con forza \italic { p, i, m } sulle corde n.6,
        5 e 4, in modo che battano contro la tastiera.
      }
    }
  r4^"perc"
  }
  \tag #'nofoot { r4 }


E specificare quale tag tenere **in ogni singola voce**::

  \score {
  \new StaffGroup <<
    \new Staff = "guitar" <<
      \context Voice = "first voice" { \clef "G_8" \voiceOne \keepWithTag #'foot  \first }
      \context Voice = "second voice" { \clef "G_8" \voiceTwo \keepWithTag #'foot  \second }
    >>
    \new TabStaff = "tab" <<
      \context TabVoice = "tab first voice" { \clef "moderntab" \voiceOne \keepWithTag #'nofoot \first }
      \context TabVoice = "tab second voice" { \clef "moderntab" \voiceTwo \keepWithTag #'nofoot \second }
    >>
  >>
  }



Midi
====

Qsynth
------

Per i Midi preferisco usare Qsynth (perché timidity viene spesso bloccato
da qualche applicazione e non viene visto da PulseAudio)::

    aptitude install qsynth


Nelle impostazioni il driver deve essere impostato su Pulseaudio o Alsa (invece di jack).
Poi bisogna caricare i soundfont.  Vediamo cosa abbiamo a disposizione::

    find / -name '*.sf2'

Di solito uso i soundfont usati anche da MuseScore, ovvero ``/usr/share/sounds/sf2/TimGM6mb.sf2``.

Conviene anche impostare il *Midi client name* non su un valore variabile
come il pid (identificativo di processo) ma su un valore fisso (qsynth).
In questo modo non ci sarà bisogno di scegliere ogni volta il dispositivo
Midi nelle preferenze di Frescobaldi.


JACK
----

JACK sarebbe la scelta migliore, ma non è ancora supportato dal MIDI player
di Frescobaldi: https://github.com/wbsoft/frescobaldi/issues/40

Nelle connessioni di Jackctl manca infatti l'applicazione Frescobaldi, che
andrebbe collegata al playback Midi.  MuseScore ad esempio lo supporta già:
http://musescore.org/node/3267



MIDI e LilyPond
---------------

Lo strumento Midi si può inserire all'interno di un blocco Staff::

    \new Staff \with {
      midiInstrument = "acoustic guitar (steel)"
    }

La lista di tutti gli strumenti supportati da LilyPond è disponibile in
un'appendice della documentazione (Notation Reference).

Se il brano viene suonato con un capotasto mobile, si può fare che il Midi
suoni come il brano originale.  Basta trasporre soltanto il blocco \\score
per il Midi di quanti toni si vuole.  Ad esempio, in questo caso ho un
capotasto al quarto tasto e traspongo di due toni::

    \score {
      \unfoldRepeats \articulate \transpose c e \music
      \midi {
        \tempo 4 = 180
      }
    }


LSR snippet
===========

Il Lilypond Snippet Repository (LSR) è una raccolta di snippet (frammenti
di codice) che permettono di ottenere risultati speciali da LilyPond.
Contengono spesso codice Scheme.

Snippet interessanti:

- inserire la data del giorno nell'header: http://lsr.dsi.unimi.it/LSR/Item?id=563
- barré per chitarra: http://lsr.dsi.unimi.it/LSR/Snippet?id=632



Mutopia Project
===============

Un ottimo modo per imparare LilyPond e allo stesso tempo contribuire alla
crescita della Cultura Libera è contribuire al progetto Mutopia, una
biblioteca di partiture nel pubblico dominio o rilasciate con licenze
libere.  Si può contribuire in vari modi:

- aggiornare i file esistenti all'ultima versione stabile di LilyPond
- aggiungere un nuovo spartito (preso da una biblioteca della propria
  città o da qualche sito online che offre scansioni di spartiti)
- contribuire al miglioramento della piattaforma web

Il repository principale si trova su GitHub:
https://github.com/chrissawer/The-Mutopia-Project

Il modo migliore per contribuire, se si ha un account GitHub, è forkare
il repository e lanciare una *pull request*:

- https://help.github.com/articles/fork-a-repo
- https://help.github.com/articles/using-pull-requests



Teoria musicale
===============

- http://www.dolmetsch.com/theoryintro.htm
- https://archive.org/details/EssentialDictionaryOfMusicNotation


Bookmark
========

- http://packages.python.org/Abjad/
- https://github.com/BertrandBordage/Titelouze
- http://www.lilybin.com/
- http://weblily.net/
- http://tunefl.com/
- http://www.omet.ca/
- https://github.com/openlilylib/lilyglyphs (compreso nel pacchetto texlive-music)
- http://code.google.com/p/ly2video/
- https://pypi.python.org/pypi/digue

