*********
MuseScore
*********

`MuseScore <http://musescore.org/>`_ non è un front-end di LilyPond ma
un programma del tutto separato.  Il suo output può sembrare simile
perché usa il font Emmentaler di LilyPond.  Ha comunque la possibilità
di esportare in formato LilyPond.

Mi prendo qualche appunto sul suo utilizzo, perché, se si considerano i
programmi a interfaccia grafica, è al momento l'unica vera alternativa
ai grandi software commerciali.



Installazione
=============

I repository delle principali distribuzioni Linux hanno l'ultima stabile,
ma a me interessa sperimentare quella che sarà la versione 2.0, perché
supporta la tablatura.

L'opzione più semplice è usare un binario precompilato delle "nightly build":
http://prereleases.musescore.org/linux/nightly/

Si scompatta l'archivio, si rende il file eseguibile e si clicca.

Per eseguire il binario della versione di sviluppo servono anche queste
dipendenze (oltre a quelle individuate da ``apt-get build-dep musescore``)::

    aptitude install libqt5core5 libQt5Gui5 libQt5Network5 libqt5xml5 libqt5xmlpatterns5 libqt5svg5 libqt5printsupport5 libqt5webkit5


Pacchetti che ho dovuto installare in tentativi di compilazione più recenti::

    libqt5designer5 libqt5svg5-dev libqt5webkit5-dev libqt5xmlpatterns5-dev
    libqt5declarative5 libqt5script5 libqt5scripttools5

Altrimenti si può compilare dai sorgenti::

    git clone git://github.com/musescore/MuseScore.git
    make release
    su -c 'make install'

.. NOTE::
   Verifica di avere installato tutte le dipendenze elencate qui:
   http://musescore.org/en/developers-handbook/compilation/compile-instructions-ubuntu-12.04-git

L'eseguibile è ``/usr/local/bin/mscore``.

Per compilare il sorgente aggiornato::

    git pull
    rm -rf build.release
    make release
    su -c 'make install'



Documentazione
==============

Manuali e screencast:

- http://musescore.org/en/handbook/handbook
- http://www.youtube.com/user/MuseScoreHowTo/videos?view=0



Tablatura
=========

Il wizard per la creazione dello spartito permette di creare qualsiasi tipo
di partitura.  Il tipico spartito rigo + tablatura si crea così:

- prima si aggiunge un rigo normale
- poi si aggiunge un rigo collegato e si sceglie Tablatura

Quando i righi sono collegati, se si inserisce una nota in un rigo apparirà
anche nell'altro.

Il nuovo modello si può salvare nel percorso indicato nelle Preferenze.
Nel mio caso è ``/home/fede/Documenti/MuseScoreDevelopment/Templates``.

Vedi anche http://musescore.org/en/node/7845
