.. _Audacity: http://audacity.sourceforge.net/
.. _controllare i livelli: http://wiki.audacityteam.org/wiki/Recording_levels

********
Audacity
********

`Audacity`_ è un software di registrazione audio di facile utilizzo.

Risorse:

- http://audacityteam.org
- http://wiki.audacityteam.org/wiki/Recording_Tips (partire da qui!)

Audacity può essere usato con Jack, se si avvia Jack prima di lanciarlo.
Jack comparirà tra le altre opzioni (Alsa, OSS) nel menu a tendina della
barra dispositivi.

Prima di iniziare a registrare conviene `controllare i livelli`_.

Mentre il livello dell'output (icona dell'altoparlante) è monitorato di
default, quello dell'input (icona microfono) deve essere attivato cliccando
su *Avvia monitoraggio*.  Se suonando lo strumento si vede il segnale, allora
l'hardware è ben configurato.  Nelle Preferenze si può abilitare il *Software
Playthrough* per sentire come viene riprodotto l'input.


