Fotografia
==========

.. toctree::
   :maxdepth: 1

   fondamentali
   metadati
   darktable
   gimp
