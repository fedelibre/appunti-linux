.. _modulo pyexiv2: http://tilloy.net/dev/pyexiv2/

***********************
Metadati di un'immagine
***********************

Esistono tre principali standard per la gestione dei metadati delle foto:

- `EXIF <http://en.wikipedia.org/wiki/Exif>`_, lo standard più diffuso e utilizzato
- `XMP <http://en.wikipedia.org/wiki/EXtensible_Metadata_Platform>`_, sviluppato
  dalla Adobe
- `IPTC <http://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model>`_,
  ormai sostituito da XMP

Per modificare i metadati si usa `exiv2 <http://www.exiv2.org/>`_ o il modulo
python `pyexiv2 <http://tilloy.net/dev/pyexiv2/index.html>`_, che permette
di scrivere degli script python per la modifica dei metadati.

È bene conoscere la `lista dei metadati supportati da EXIF <http://www.exiv2.org/tags.html>`_.


Exiv2
=====

Si possono vedere tutti i tag Exif presenti in un'immagine con questo comando::

   exiv2 -pt immagine.jpg

Per isolare un solo tag di tante immagini si usa grep.  Per esempio, per vedere
la lunghezza focale usata in un rullino::

    exiv2 -pt *.NEF | grep Exif.Photo.FocalLength | grep Rational

Vediamo ora come modificare i tag Exif più utilizzati.


Data
----

Per modificare la data dello scatto occorre specificare lo scarto (positivo o
negativo) per arrivare alla data che si vuole.  Nel seguente esempio la data
24/01/03 è diventata 20/09/07::

   exiv2 ad -Y 4 -O 8 -D -4 20030124.jpg

Le opzioni sono:

- ``-Y 4``: va avanti 4 anni, quindi da 2003 a 2007
- ``-O 8``: va avanti di 8 mesi, quindi da gennaio va a settembre
- ``-D -4``: va indietro di 4 giorni, dal 24 al 20

Naturalmente questa modifica può essere eseguita su più file usando il carattere
wildcard ``*``::

   exiv2 ad -Y 4 -O 8 -D 4 *_01gen2003.jpg

Se c'è un'ora sbagliata si può correggere con l'opzione ``a``::

   exiv2 ad -a 10:06 file.jpg

Che aumenterà l'ora di 10 ore e 6 minuti.  Col meno si diminuisce.


Nome file
---------

Dopo aver fatto le modifiche si possono rinominare i file in modo che l'ordine
dei nomi dei file rispecchi la sequenza della data di scatto::

   exiv2 rename *.jpg


Il formato predefinito per rename è YYYYMMDD_HHMMSS, ovvero
"annomesegiorno_oraminutisecondi".  Ad esempio: 20071005_001025.


Autore
------

L'autore della foto è identificato dal tag ``Exif.Image.Artist``.  Lo si
può modificare in questo modo::

   exiv2 -M"set Exif.Image.Artist 'Nome Cognome'" *.jpg

Per verificare::

   exiv2 -pt *.jpg | grep Image.Artist


File esterno
------------

Se si vogliono modificare più tag, conviene scriverli in un file di
testo, chiamato ad esempio ``cmd.txt``::

   add  Exif.Image.Artist              "Nome Cognome"
   add  Exif.Image.Make                Ascii   "Canon"
   add  Exif.Image.Model               Ascii   "Canon PowerShot G7"
   add  Exif.Photo.DateTimeOriginal	   Ascii   "2009:09:27 23:20:45"
   set  Exif.Image.ImageDescription    Ascii   "Titolo"


e poi applicarle in una sola volta con questo comando::

   exiv2 -m cmd.txt *.jpg


.. NOTE::
   Si usa ``set`` per modificare i valori di tag esistenti e ``add`` per
   aggiungere un tag non presente.




Pyexiv2
=======

Le modifiche più difficili sono quelle che richiedono un'analisi dei metadati di
ogni immagine prima di decidere quale modifica applicare.  Ad esempio, potremmo
voler inserire i nomi degli autori di tante foto suddivise in varie directory.
Sappiamo che a ciascun diverso tipo di macchina corrisponde un autore.  Come
applicare il tag giusto con un solo comando?
Ci viene in aiuto il `modulo pyexiv2`_.

.. TODO::
   Scrivere uno script python che sfrutti il modulo pyexiv2.


