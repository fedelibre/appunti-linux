****
Gimp
****

Supporto:

- http://www.youtube.com/user/dOOnLoL (eccellenti screencast)
- http://gimpitalia.it/forum/
- http://www.mora-foto.it/tutorial_gimp/tutorial_gimp.html
- http://www.dorianorossi.com/freeliving/opensource-e-grafica/95-dorylinux-mega-raccolta-di-video-tutorial-e-tutorial-per-gimp


Gimp vs Photoshop
=================

Dibattito infinito...  Photoshop resta un programma superiore, tuttavia spesso
questa superiorità viene argomentata in modo sbagliato:

Quadricromia
------------

Il grande limite di Gimp rispetto a Photoshop non è la quadricromia.  Infatti
Gimp serve a modificare le fotografie e per farlo bisogna lavorare in RGB.
Il passaggio da RGB a CMYK riguarda la gestione del colore e questa viene
fatta anche attraverso software diversi da Gimp.  In ogni caso la questione
della quadricromia si pone in fase di stampa oppure quando si lavora su delle
immagini destinate ad essere importate in un software di desktop publishing.


Modifiche non distruttive
-------------------------

Ecco ciò che rende (per ora) superiore Photoshop.
L'introduzione di questa caratteristica in Gimp dipende dallo sviluppo di
GEGL.  Forse sarà introdotto nella versione 3.



Sviluppo RAW
============

Talvolta lo sviluppo di una foto richiede funzionalità che gli attuali
editor RAW non supportano.  In questo caso bisogna usare Gimp.  Ma Gimp non
è nato per lo sviluppo di file RAW, dunque bisogna sfruttare dei plugin.

Ne esistono due: ``gimp-dcraw`` e ``gimp-ufraw``.  Nei sistemi GNU/Linux
questi due pacchetti sono in conflitto, ovvero solo uno di questi può essere
installato.  Il consiglio è di usare ``gimp-ufraw``, perché dcraw si limita
a caricare l'immagine RAW definendo alcuni (pochi) parametri come il
bilanciamento del bianco e la demosaicizzazione.  Mentre il plugin ufraw
permette di sfruttare il programma standalone Ufraw per ritoccare l'immagine
con uno strumento specifico per file RAW.  Poi si può passare il lavoro a
Gimp.



gimp-ufraw
----------

Installazione::

   aptitude install gimp-ufraw


Quello che non si comprende immediatamente è che pur essendo un plugin di
Gimp, ``gimp-ufraw`` non è incorporato nell'interfaccia di Gimp, bensì si
limita a far sì che il lavoro fatto da Ufraw possa essere trasferito in Gimp.

Quindi il tipico workflow dello sviluppo di file RAW in Gimp è:

#. aprire il file con Ufraw
#. modificare le proprietà specifiche del file RAW (bilanciamento del bianco,
   esposizione, ....)
#. cliccare sul logo di Gimp in basso a destra quando l'immagine è pronta per
   l'editing extra che ha reso necessario l'uso di Gimp

