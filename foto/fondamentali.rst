.. _RFP per gnome-raw-thumbnailer: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=670699
.. _Shotwell: http://www.yorba.org/projects/shotwell/
.. _protocollo PTP: http://en.wikipedia.org/wiki/Picture_Transfer_Protocol
.. _Rapid Photo Downloader: http://damonlynch.net/rapid/


***************************************
Collegamento fotocamera e gestione file
***************************************

Scaricare i file nel filesystem
===============================

Alcune fotocamere supportano la modalità *USB mass storage* e, se questa è
abilitata, si possono trasferire le foto direttamente col gestore file.  Se
invece la fotocamera supporta solamente il `protocollo PTP`_, conviene usare
il pacchetto ``gphoto2``.  Ecco la serie di comandi che uso::

    mkdir Immagini/album && cd Immagini/album
    gphoto2 -P
    exiv2 rename *
    gphoto2 -DR  # per eliminare i file è necessario specificare la ricorsione

.. WARNING::
   Sconsiglio l'uso del pacchetto ``gphotofs``, che dà problemi coi permessi.

Una volta che i file sono sul filesystem, si possono comunque usare programmi
più avanzati per rinominare e spostare i file, come `Rapid Photo Downloader`_
o PyRenamer.



Anteprime raw sul gestore file
==============================

Nautilus attualmente non riesce a visualizzare l'anteprima dei file raw, per
cui occorre compilare ``gnome-raw-thumbnailer``. Ho già aperto una
`RFP per gnome-raw-thumbnailer`_ (richiesta di pacchettizzazione per Debian).

Per compilare installiamo prima le dipendenze::

    aptitude install libopenrawgnome-dev

Scarichiamo e compiliamo i sorgenti::

    git clone git://git.gnome.org/gnome-raw-thumbnailer
    cd gnome-raw-thumbnailer
    ./autogen.sh
    make
    sudo make install

Basta riavviare Nautilus per vedere le anteprime.



Gestori fotografie
==================

- `Shotwell`_ (desktop GNOME)
- `gnome-photos <https://wiki.gnome.org/Design/Apps/Photos>`_
