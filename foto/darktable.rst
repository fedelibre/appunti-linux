.. _darktable: http://www.darktable.org/
.. _Picasa Web Album: https://picasaweb.google.com/
.. _Google+: https://plus.google.com/
.. _Picasa free storage limits: http://support.google.com/picasa/bin/answer.py?hl=en&answer=1224181

*********
Darktable
*********

`darktable`_ è un software che permette di gestire e di sviluppare le foto
in formato RAW (i cosiddetti negativi digitali).  Il nome stesso nasconde
la sua doppia funzione: dark(room) + (light)table, ovvero la camera oscura
per lo sviluppo fotografico e la lavagna luminosa per la selezione dei
negativi.

Per poter lavorare in modo consapevole alle immagini RAW servono delle
conoscenze di base che non sono specifiche del programma.  Qualche utile
risorsa online:

- http://www.cambridgeincolour.com/tutorials.htm
- http://graphics.stanford.edu/courses/cs178-11/


Il tipico flusso di lavoro con Darktable prevede varie fasi: importazione,
applicazione di metadati e tag, valutazione e selezione delle foto,
sviluppo, esportazione.  La maggior parte di queste avviene all'interno
della lavagna.


Tutorial
========

Screencast:

- http://www.youtube.com/user/rhutton86/videos



Lavagna
=======

Importazione
------------

Darktable permette di gestire il workflow fotografico fin dall'inizio,
quando si importano i file dalla scheda SD della fotocamera al disco
del computer.  Si tratta di un passaggio delicato, perché bisogna
fare l'importante scelta della struttura e del nome dei file nel
filesystem.
Qualche buona idea si può trarre da questo screencast:
http://blog.pcode.nl/wp-content/uploads/2010/12/darktable-lighttable-tagging-collect.ogv

.. NOTE::
   Attualmente Darktable non è in grado di correggere il percorso di file
   che siano stati spostati o rinominati.  Ci sono due soluzioni:

   #. cancellare il database e reimportare la cartella con tutti i rullini
      (si perdono solo i preset)
   #. cercare le immagini che il database ha perso, rimuoverle dal database
      e poi reimportare solo le immagini spostate/rinominate (non si perde niente)

..  Import from camera: commentata finché non viene introdotto il supporto ai metadat Exiv
    Per importare le foto si inserisce la scheda SD e si clicca su *scan for
    devices*.  Comparirà il pulsante *Import from camera*.  Si sceglie il jobcode,
    ovvero un nome che identifica il rullino da importare e nelle impostazioni conviene:

    - ignorare i file jpeg
    - cancellare gli originali dopo l'importazione
    - storage directory: `/media/disco_esterno` ($(PICTURES_FOLDER) corrisponde a `~/Immagini`
      e non conviene usarlo perché altri programmi di gestione delle immagini potrebbero
      usare questa stessa directory per l'importazione)
    - directory structure: $(YEAR)/$(JOBCODE)
    - filename structure: $(YEAR)$(MONTH)$(DAY)_$(SEQUENCE).$(FILE_EXTENSION)

    Quindi i miei rullini saranno ragruppati per anno/nome_rullino/data_sequenza.estensione

Finché gphoto2 non supporta la gestione dei metadati Exif non sarà possibile
importare delle foto usando questi metadati per creare il nome del file:
http://darktable.org/redmine/issues/8415

L'unica soluzione al momento è spostare manualmente i file dalla scheda SD
all'hard disk e rinominarli con ``rapid-photo-downloader`` o con ``exiv2``::

    exiv2 rename *

Quindi si importa la cartella.  Conviene ignorare i file jpeg e
disabilitare l'importazione ricorsiva, in modo da escludere l'eventuale
directory in cui si possono spostare i file JPEG generati dalla macchina
(possono essere utili per un confronto con quel che si è in grado di
sviluppare dal RAW).



Salvataggio dati: file XMP e database
-------------------------------------

Dove vengono salvati i dati?  Questa è la domanda più importante da farsi fin
dall'inizio per evitare problemi in futuro.  Ci sono essenzialmente due modalità
di memorizzazione:

- file XMP: di default Darktable scrive in questi file tutte le modifiche
  relative a un'immagine (coda di sviluppo, metadati, tag, rating).  Queste
  informazioni non vengono perse se il database diventa inutilizzabile.
  I file sidecar, indispensabili per associare le informazioni di sviluppo
  fotografico a un file RAW (che non può e non deve essere modificato), hanno
  il vantaggio di permette lo sviluppo collaborativo di una foto.

- database sqlite: è il database specifico dell'applicazione e serve
  prevalentemente a gestire le informazioni peculiari di Darktable: quali
  sono i rullini importati, i preset, etc.  Il database predefinito si trova
  in ``.config/darktable``, che contiene anche la cartella degli stili salvati
  e il file di configurazione darktablerc.


Metadati
--------

Si selezionano le foto a cui applicare i dati, si riempiono i campi dell'editor
dei metadati e si applicano alle foto.  I campi più importanti sono:

- autore (creator) » tag Exif.Image.Artist
- descrizione » tag Exif.Image.ImageDescription.  Praticamente
  tutte le applicazioni che conosco (Picasa, Lazygal, EoG, Nautilus, etc.) prendono
  le informazioni sulla foto da questo tag.

Riferimenti: :doc:`metadati`


Tag
---

I tag sono le parole chiave che vogliamo associare a una o più immagini.
Un tag isolato viene considerato come non classificato, mentre quelli
inseriti nella forma `categoria|sottocategoria` saranno strutturati.

Darktable gestisce in modo intelligente l'esportazione dei tag verso
le gallerie web come Picasa o Flickr, infatti:

- rimuove i tag generati da Darktable per finalità interne (come `darktable|changed`
  o `darktable|format|nef`)
- spezza i tag strutturati in singoli tag


Valutazione
-----------

Quando si scattano tante foto per un singolo rullino (ovvero per un soggetto/evento),
è indispensabile filtrare ciò che vale la pena sviluppare con attenzione.
Normalmente lavoro in questo modo:

- nelle preferenze metto la spunta a "don't use embedded preview jpeg but
  half-size raw"
- nella lavagna, modalità file manager, imposto un'immagine per volta e clicco
  il tasto tab per nascondere tutti i pannelli laterali
- F11 per andare a schermo intero

Ho una perfetta visione dell'immagine e non mi resta che fare le scelte
usando le scorciatoie da tastiera:

- 0 = nessuna valutazione
- 1-5 = da 1 a 5 stelle
- r = rifiutata
- canc = cancellata dal database (ma non dal disco)

.. NOTE::
   Le scorciatoie possono essere personalizzate nelle preferenze, nel tab
   *shortcuts*.  Potrebbe essere utile una scorciatoia per eliminare il file:
   si va in *modules»selected images*, doppio clic sulla funzione *delete from disc*
   e si digita la scorciatoia, ad esempio Ctrl + Canc.

Si possono inserire le valutazioni anche in modalità darkroom, cliccando sulle
stelle presenti nelle anteprime del `film strip` in basso.


Esportazione
------------

Per lo sviluppo riservo la sezione successiva.  Vediamo l'ultima parte del
workflow: l'esportazione.

L'esportazione sul proprio computer richiede solamente la comprensione
delle variabili (basta passare col mouse sul campo per vedere il tooltip
con le opzioni possibili e il loro significato).  Solitamente uso
``$(FILE_FOLDER)/darktable_exported/$(FILE_NAME)``.

Nell'esportazione web bisogna per prima cosa inserire i dati di accesso al
proprio account.  Quindi si possono salvare le immagini in un album esistente
o in un nuovo album.

Conviene mettere una spunta su "export tags": i tag generati da Darktable (come
darktable|changed) non verranno esportati.

.. NOTE::
   I tag sono visibili solo su `Picasa Web Album`_ e non su `Google+`_ (dove
   solo i *face tag* funzionano, al momento).

Le dimensioni massime per caricare immagini su Picasa senza intaccare il proprio
spazio disco gratuito è 2048x2048 (vd `Picasa free storage limits`_).

Per non doverselo ricordare ogni volta, conviene impostare un preset per
l'esportazione Picasa.

.. WARNING::
   Il preset viene salvato nel database.  Quindi se il database si corrompe
   si perdono i preset.  Controlla la `feature request 8585 <http://darktable.org/redmine/issues/8585>`_.

Per migliorare la qualità dei file esportati bisogna mettere una spunta
nelle preferenze in *do high quality resampling during export*.  Questa opzione
è spiegata nel blog di Darktable: `High Quality Resampling <http://www.darktable.org/2012/06/upcoming-features-new-interpolation-modes-and-better-resize/>`_



Camera oscura
=============

Nella camera oscura ha luogo lo sviluppo del negativo (digitale).
I moduli possono essere disattivati (grigio scuro), attivati (grigio chiaro) e
impostati come preferiti (rosso).
I gruppi sono organizzati in gruppi (nella barra superiore) per facilitare
l'accesso ad essi.  Vediamo ciascun gruppo.


Moduli attivi
-------------

Serve ad accedere rapidamente ai moduli usati per sviluppare l'immagine.
È molto utile per capire quali sono le modifiche predefinite che Darktable
applica a un'immagine.

La più importante di queste è senza dubbio la **Curva base**, che migliora
il contrasto e i colori.  Darktable attiva in automatico il preset specifico
della macchina fotografica usata.  In pratica, permette di avere un file
raw che ha già una qualità migliore rispetto allo scatto raw nudo e crudo.


Preferiti
---------

Cliccando una volta in più su un modulo attivo, il modulo diventa preferito.
È utile raggruppare nei preferiti i moduli appartenenti a gruppi diversi
e che usiamo più spesso, in modo da avere accesso più facilmente a ciascuno
di essi.


Base
----


Toni
----

Shadows and highlights:
http://www.darktable.org/2012/02/shadow-recovery-revisited/


Colore
------

Isolare un colore e trasformare il resto in bianco e nero (plugin Monocromia + maschere):
http://www.youtube.com/watch?v=xWF6Wk-JPb0


Correzione
----------

Equalizer: il preset "Sharpen and denoise" è ottimo per ridare contrasto e
togliere un po' di rumore senza sacrificare la qualità dell'immagine.  Lo uso
sempre nelle foto dei concerti.

Anche il preset "Clarity (subtle)" dà ottimi risultati senza stravolgere la foto.

Denoise
~~~~~~~

Riferimenti: `screencast sul denoise in Darktable <http://blog.pcode.nl/wp-content/uploads/2011/10/22/darktable-denoise.ogv>`_

Ci sono vari moduli per togliere il rumore a un'immagine, in ordine decrescente
di preferenza:

- **denoise profiled**: è fatto su misura per i vari modelli di fotocamera.
- **denoise (bilateral filter)**: consuma molta memoria ed è consigliato solo
  per macchine a 64-bit e con molta memoria RAM
- **denoise (non local means)**: leggero e efficace.  Luma serve a correggere la
  luminosità, chroma corregge i colori.  Il `chroma noise` è quello che dà
  maggiormente fastidio all'occhio (le macchioline colorate nelle zone
  dell'immagine poco esposte).

La rimozione del rumore implica sempre un po' di perdita di definizione
dell'immagine.  Si può compensare questa perdita con i moduli **sharpen**
e **equalizer** (clarity, ad esempio).


Effetti
-------


HDR
---

Si esporta l'immagine in TIFF a 16 bit e si processa col programma ``luminance-hdr``.
Poi si importa l'immagine HDR in Darktable e si seguono le modifiche
consigliate in questo post:
http://www.darktable.org/2012/10/process-hdr-images-using-darktable/



Supporto
========

Irc, mailing list, blog:

- IRC Freenode: #darktable
- http://sourceforge.net/search/?group_id=258690&type_of_search=mlists
- http://www.darktable.org/category/blog/

Manuali:

- http://www.darktable.org/usermanual/index.html.php (manuale ufficiale)
- http://stefanofornari.github.com/darktable-book/index.html (libro di alcuni utenti)

Bug:

- http://darktable.org/redmine/projects/darktable/wiki/Bug_Workflow
- ``darktable -d all``, (per la versione debian è necessario il pacchetto darktable-dbg)
- ``gdb darktable``: http://blog.pcode.nl/2010/08/31/contributing-backtraces/



Installazione
=============

darktable è presente nei repository Debian, quindi si installa con un
semplice::

   aptitude install darktable


Per compilare la versione di sviluppo::

   sudo apt-get build-dep darktable
   git clone git://github.com/darktable-org/darktable.git
   cd darktable
   ./build.sh --prefix ~/usr/darktable
   cd build
   make install

Di default vie installato in /opt, ma preferisco installarlo in una directory
in cui il mio utente abbia diritto di scrittura, così da non dover avere i
privilegi di root.  Entrambi i percorsi però non sono inclusi nella variabile PATH.
Dato che tengo sempre la versione stabile dei repository Debian, preferisco
creare un eseguibile con un nome diverso, ovvero ``darktable-git`` (che preferisco
usare con interfaccia inglese, vd sotto).

Per aggiornare la versione di sviluppo::

    cd build
    make uninstall
    cd ../
    rm -rf build
    git pull
    ./build.sh
    cd build
    make install

.. NOTE::
   Nel caso in cui si stia per comprare un nuovo computer...
   Darktable richiede molte risorse di elaborazione ed è quindi preferibile
   avere un processore a 64 bit: http://www.darktable.org/2012/03/darktable-and-memory/

Il branch predefinito del repository è master, ovvero il branch della versione
instabile.  Se si desidera scaricare l'ultima versione disponibile di un
branch stabile, ad esempio della serie 1.0::

    git branch -a
    git checkout -b darktable-1.0.x origin/darktable-1.0.x

E poi seguire le istruzioni precedenti per la compilazione.


Lingua
------

Un modo semplice per cambiare lingua per una sola sessione è questo::

   export LANG=C
   <percorso a>/darktable

Questo può essere utile quando si segue un tutorial di un utente che usa
l'interfaccia inglese e soprattutto se si vuole chiedere aiuto in una
mailing list internazionale.

Ancora più comodo è rendere la modifica permanente con uno script bash
contenente queste linee:

.. code-block:: bash

   #!/bin/sh
   export LANG=C
   ~/usr/darktable/bin/darktable

Si salva in una directory che faccia parte del proprio PATH, ad esempio
``~/bin/darktable-git``.  Il file deve essere eseguibile::

   chmod u+x ~/bin/darktable


OpenCL
------

Se non si dispone di un computer recente e performante potrebbe convenire usare
OpenCL, che permette di sfruttare il processore della scheda audio (GPU) per
rendere il programma più veloce.

Prima bisogna verificare se la propria scheda audio supporta OpenCL::

   lspci | grep VGA

Individuato il modello della scheda, si fa una ricerca nel sito del produttore
e si vede nelle specifiche se OpenCL è supportato.
Un'altra fonte attendibile è Wikipedia.  Ad esempio, per le schede ATI:
http://en.wikipedia.org/wiki/Comparison_of_AMD_graphics_processing_units

Se la scheda è supportata bisogna installare i driver proprietari e un
pacchetto specifico per OpenCL:

- http://wiki.debian.org/ATIProprietary
- http://wiki.debian.org/ATIStream

.. NOTE::
   Al momento il pacchetto ``amd-libopencl1`` si trova solo in unstable, ma
   col pinning si può installare.

Il supporto a OpenCL è ancora sperimentale in Darktable, dunque deve essere
abilitato esplicitamente nelle Preferenze.  Se abilitato, Darktable verifica
se ci sono le condizioni per l'uso di OpenCL e in caso di problemi disattiva
OpenCL.

.. TODO:: mi fermo qui perché la mia attuale scheda video non è supportata


Riferimenti:

- http://www.darktable.org/2012/03/darktable-and-opencl/
- http://en.wikipedia.org/wiki/OpenCL#Drivers
- http://www.thebigblob.com/getting-started-with-opencl-and-gpu-computing/



Varie
=====

Gimp
----

Gimp spesso viene considerato uno strumento indispensabile perché ha degli
strumenti di fotoritocco che non sono ancora presenti nei software RAW.  Ma
c'è il problema di come mettere in comunicazione i due software.

`Questo post <http://sourceforge.net/apps/wordpress/darktable/2011/03/28/why-gimp-doesnt-play-well-with-darktable/>`_
spiega perché non ha senso al momento avere la possibilità di esportare un'immagine
da DarkTable a Gimp.  Gimp non supporta ancora i cambiamenti non distruttivi né
la profondità di colore a 16 bit, quindi esportare un'immagine da DT a Gimp
significherebbe perdere tutte queste informazioni.


