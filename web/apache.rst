******
Apache
******


Risorse:

- http://guide.debianizzati.org/index.php/LAMP:_Linux,_Apache,_MySQL_e_PHP
- http://httpd.apache.org/docs/2.0/

DocumentRoot
============

La DocumentRoot è la directory in cui si trovano i file che Apache
può servire.  È quella che si vede in http://localhost/

Io preferisco spostarla nella home, per risparmiare spazio alla
partizione root / e per semplificare i backup (tutti i dati
sono in /home).

Aggiungere il file ``/etc/apache2/sites-available/locale.conf`` e
inserire queste linee::

    <VirtualHost *:80>
      ServerName localhost
      DocumentRoot /home/fede/public_html
      <Directory /home/fede/public_html>
        Options Indexes FollowSymLinks MultiViews
        Order allow,deny
        Allow from all
      </Directory>
    </VirtualHost>


Disattivare il sito predefinito, attivare quello nuovo e
riavviare Apache::

    a2dissite 000-default
    a2ensite locale
	service apache2 reload


Se proprio si vuole usare la directory predefinita, conviene
semplificare la modifica dei file aggiungendo il proprio utente
al gruppo `www-data` per avere permesso di scrittura in /var/www::

	usermod -G www-data -a fede

Occorre chiudere la sessione e rifare il login perche' le modifiche abbiano
effetto. Verificare con::

	groups


Se /var/www appartiene a root:root, occorrerà modificarlo::

	chown -R fede:www-data /var/www




.htaccess
=========

Apache legge prima i file di configurazione globale apache2.conf e httpd.conf e
poi cerca nelle directory di ogni progetto eventuali file di configurazione specifici.
Questi file vengono chiamati convenzionalmente .htaccess.

Questo nome in realtà è definito nel file di configurazione di Apache::

    cat /etc/apache2/apache2.conf | grep AccessFileName

La direttiva `AllowOverride <http://httpd.apache.org/docs/2.2/mod/core.html#allowoverride>`_
di default ha valore All, dunque i file .htaccess possono sovrascrivere qualsiasi
direttiva.



Configurazione multisito
========================

Un unico server (un solo IP) può gestire molteplici siti grazie ai
VirtualHost.

.. TODO:: aggiungere spiegazione




Aprire Apache ai computer della LAN
===================================

Per far sì che il mio server apache sia visibile solo ai computer
della LAN, modificare il file in un modo simile a questo::

	<Directory /home/fede/public_html>
	  Order allow,deny
	  Allow from 10.0.20
	  Allow from localhost
	</Directory>

dove 10.0.20 sono i primi tre numeri dell'IP privato che identifica tutte le
macchine connesse alla LAN.

.. NOTE::
	Se avessi messo 'allow from all' i contenuti sarebbero stati visibili,
	almeno potenzialmente, a degli intrusi con IP pubblico.
	'Allow from localhost' dà il diritto di accesso al computer in cui
	risiede il server web.


Per rendere le modifiche effettive si riavvia Apache::

	service apache2 reload


Maggiori informazioni sulle restrizioni di accesso:

- http://httpd.apache.org/docs/2.2/mod/mod_authz_host.html#allow
- http://wiki.apache.org/httpd/BypassAuthenticationOrAuthorizationRequirements

