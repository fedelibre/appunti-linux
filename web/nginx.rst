_nginx: http://nginx.org/

*****
nginx
*****

Installazione
=============

`nginx`_ è un server web leggero e quindi particolarmente adatto ai server
VPS.  La versione presente nei repository di Debian stable è solitamente
vecchia e conviene quindi mettere questi repository in `/etc/apt/sources.list`::

    deb http://nginx.org/packages/debian/ squeeze nginx
    deb-src http://nginx.org/packages/debian/ squeeze nginx

Quindi si scarica la chiave che autentica i pacchetti del repository, si aggiorna
la lista dei pacchetti e si installa nginx::

    wget http://nginx.org/keys/nginx_signing.key
    apt-key add nginx_signing.key
    aptitude update
    aptitude install nginx



Configurazione
==============

I siti gestiti da nginx vengono specificati nei file creati nella
directory `/etc/nginx/conf.d`.
La direttiva più importa da conoscere è
`server_name <http://nginx.org/en/docs/http/server_names.html>`_.

Ecco l'esempio del file di questo sito::

    server {
        listen	80;
        server_name	www.fedelibre.org;
        return	301 http://fedelibre.org$request_uri;
    }

    server {
        listen	80;
        server_name fedelibre.org;

        access_log /var/log/nginx/fedelibre.org/access.log;

        location / {
            root /home/fede/public_html/fedelibre.org;
            index index.html index.htm;
        }
    }


