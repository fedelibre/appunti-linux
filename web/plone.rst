*****
Plone
*****

Documentazione della comunità:

- http://collective-docs.readthedocs.org/en/latest/index.html


Installazione
=============

Dipendenze di sistema::

    aptitude install python-dev build-essential libssl-dev libxml2-dev libxslt1-dev libbz2-dev

È importante usare la stessa versione di Python in locale e sul server.  Se sul
server la versione predefinita è python2.6 e in locale è python2.7, la soluzione
più semplice è usare virtualenv per usare python2.6 in locale::

    aptitude install python-virtualenv python2.6


Si crea un virtualenv che usa python2.6::

    virtualenv  -p /usr/bin/python2.6 ~/virtualenv/plone

Non è necessario specificare l'opzione ``--no-site-packages``, che è predefinita
e impedisce all'ambiente virtuale di avere accesso ai moduli aggiuntivi
alla libreria standard installati a livello di sistema.

Si avvia il virtualenv::

    source virtualenv/plone/bin/activate

Sul terminale comparirà il nome del virtualenv (plone).  Si può verificare
che la versione di Python sia la 2.6::

    (plone)utente@macchina:~/$ python -V
    Python 2.6.8

Ora installiamo le librerie python che devono stare nell'ambiente virtuale::

    pip install pil distribute==0.6.27


Buildout
========

Forse conviene usare il buildout dell'Unified installer...

Un problema tipico è il conflitto di versione tra un egg e il zc.buildout
installato.  Per risolverlo bisogna rieseguire il bootstrap (che crea
lo script buildout) con la versione di zc.buildout richiesta::

    ../Python-2.7/bin/python bootstrap.py -v 1.5.0
    ./bin/buildout

.. NOTE::
   Conviene usare sempre la versione di Python inclusa nell'unified
   installer, per evitare problemi (certi).




Sincronizzazione
================

Per lavorare su un progetto Plone da vari computer uso Git.  Alcuni file e
directory devono essere esclusi dall'indice di Git.  Il mio  `.gitignore`
contiene queste linee::

    bin/
    develop-eggs/
    eggs/
    parts/
    var/
    buildout.cfg
    .installed.cfg
    bootstrap.py
    syncstorage.sh
    versions.cfg


Per sincronizzare il database e i file blob, conviene creare un file bash
nella root di Plone, con questi due comandi legati da &&::

    #!/bin/sh
    scp -C -o CompressionLevel=9 var/filestorage/Data.fs utente@server:<plone dir>/var/filestorage && rsync -av --compress-level=9 var/blobstorage utente@server:<plone dir>/var

Sequenza di sincronizzazione:

#. blocco l'istanza sul server
#. eseguo la sincronizzazione da locale a remoto
#. ``git pull && ./bin/buildout`` se ho modificato il buildout
#. rilancio l'istanza sul server

Riferimento:
http://plone.org/documentation/kb/copying-a-plone-site/



Performance
===========

Memoria
-------

Bisogna far sì che Zope venga riavviato periodicamente per evitare che la
memoria sia consumata del tutto.  Si lancia ``crontab -e`` con l'utente
che esegue l'istanza Plone e si inserisce qualcosa di simile a questo::

    0 5 * * 0 <plone dir>/bin/zeoserver restart && <plone dir>/bin/instance restart

``crontab -l`` elenca tutti i `cron job` dell'utente che lancia il comando.

Riferimenti:

- http://plone-regional-forums.221720.n2.nabble.com/daemon-manager-killed-by-SIGTERM-td5859756.html
- http://plone.org/documentation/faq/do-i-have-to-restart-zope-periodically/



Configurazione
==============

L'home page può puntare a un'altra cartella (ma solo nella root, non più in
profondità).  Si va in <sito>/manage (che è diverso da manage_main), si clicca
su Properties e si aggiunge la *default folder*.


Email
-----

Bisogna impostare subito l'email a cui reindirizzare i messaggi dei visitatori
del sito.  Ci sono due sistemi:

#. email esterna (richiede autenticazione)
#. localhost (e quindi si usa l'MTA installato nel server)

.. NOTE::
   Se nel campo "From:" è presente un dominio diverso da quello predefinito
   dell'MTA, il server a cui viene inviato il messaggio potrebbe bloccarlo
   per il controllo antispoofing.  La soluzione sta nell'impostare correttamente
   i record SPF del dominio.
