*********
Mezzanine
*********

`Mezzanine <http://mezzanine.jupo.org/>`_ è un CMS basato su Django.

Installazione
=============

Conviene installarlo all'interno di un ambiente virtuale, per evitare
conflitti con le librerie di sistema::

    virtualenv virtualenv/mezzanine-1.4
    virtualenv/mezzanine-1.4/source/bin/activate

.. NOTE::
   Da ora in poi si dà per scontato che tutti i comandi siano dati dall'interno
   dell'ambiente virtuale.

Si installa Mezzananine dal PyPI::

    pip install mezzanine psycopg2

Vediamo come creare un progetto iniziale.  Il database predefinito è sqlite,
che va bene per progetti semplici.  Ma noi preferiamo usare :doc:`../sviluppo/postgres`.
Creiamo quindi il database con un utente che abbia il permesso di creare
database postgres::

    createdb nome-database

Quindi si crea il progetto mezzanine::

    mezzanine-project nome-progetto

Ora dobbiamo dire a Mezzanine di usare il database appena creato.  In
``local_settings.py`` bisogna specificare la scelta di postgres, il nome
del database, l'utente e la password, e l'host (localhost, che non sembra
necessario ma se non si mette si ottiene un errore quando si crea il
database).

Infine si modifica il file ``settings.py`` per aggiungere il proprio fuso
orario e aggiungere una variabile necessaria dalla versione 1.5 di Django::

    TIME_ZONE = "Europe/Rome"
    ALLOWED_HOSTS = "localhost"

Si può popolare il database e iniziare il progetto::

    python manage.py createdb

.. WARNING::
   Bisogna creare il superutente che ci darà accesso all'admin e quindi
   all'amministrazione del sito!

Si lancia il server::

    python manage.py runserver

E si accede all'interfaccia di admin: http://127.0.0.1:8000/admin/

Per reimpostare la password dell'admin::

    python manage.py changepassword admin

Riferimenti:

- http://rosslaird.com/blog/building-a-project-with-mezzanine/



Tema
====

I temi in Mezzanine non sono altro che delle app Django e si possono avviare
con questo comando::

    django-admin.py startapp mio-tema



Riferimenti:

- http://www.rosslaird.com/blog/customizing-mezzanine/


Moduli di terze parti
=====================

Mezzanine può essere esteso usando `moduli di terze parti <http://mezzanine.jupo.org/docs/overview.html#third-party-modules>`_.


Evidenziazione sintassi
-----------------------

Conviene usare `mezzanine-pagedown <https://bitbucket.org/akhayyat/mezzanine-pagedown>`_,
un editor che supporta Pygments::

    pip install mezzanine-pagedown pygments

In ``settings.py`` si installa l'applicazione in INSTALLED_APPS e in fondo
al file si aggiungono i widget::

    INSTALLED_APPS = (
        ...
        "mezzanine_pagedown",
        ...
    )

    ##############################
    # mezzanine-pagedown widgets #
    ##############################
    RICHTEXT_WIDGET_CLASS = 'mezzanine_pagedown.widgets.PageDownWidget'
    RICHTEXT_FILTER = 'mezzanine_pagedown.filters.codehilite'


Infine si registra l'applicazione nel database::

    python manage.py syncdb  # da verificare


ma come faccio a renderlo l'editor predefinito sostituendo tinymce?



Deployment
==========

Per il deployment bisogna installare nel virtualenv anche i pacchetti
fabric e psycopg2.  È anche necessario registrare il progetto in
un repository git o mercurial **remoto**, perché verrà usato nella
fase di creazione del virtualenv sul server per scaricare i file del
progetto.

Conviene procedere per passi.

.. NOTE::
   Un programmatore può capire questi passi guardando il file fabric.py.

1. fab install:  installa tutte le dipendenze (nginx, git, postgresql, supervisor, pip, ...).
Per completare con successo questo primo passo, la sezione FABRIC di
settings.py deve aver correttamente definite le opzioni: SSH_USER,
SSH_KEYPATH, HOSTS, LOCALE.

2. fab create: crea l'ambiente virtuale, scarica il progetto git nella
directory /project dell'ambiente virtuale e crea il database  definito
nei file settings.
Le opzioni necessarie in questa fase sono: VIRTUALENV_HOME, PROJECT_NAME,
REQUIREMENTS_PATH, REPO_URL, DB_PASS, SECRET_KEY, NEVERCACHE_KEY.
settings.py deve avere definito il dominio permesso in ALLOWED_HOSTS: ad
esempio, .example.com. È consigliato definire anche TIME_ZONE, per
avitare un warning.

Ogni volta che si lancia 'fab create', se viene trovato un virtualenv
identico a quello da creare, bisogna accettare la sua eliminazione.
O lo si può fare manualmente con 'fab remove'.

3. fab deploy: crea i cronjob, riavvia il server nginx e gli altri stumenti,
fa un backup del database dentro la directory project del server, rifa un pull
del repository del progetto e lancia collectstatic.

Se qualcosa va storto, conviene guardare i file di log sul server.

Alternative al fabfile incluso in Mezzanine:

- https://github.com/natea/django-deployer

Attenzione: Fabric non sincronizza il database locale con quello di produzione,
perché è ritenuta una operazione da fare separatamente e manualmente.
Conviene usare i comandi di backup e ripristino presenti nel fabfile.py, ovvero::

    pg_dump -Fc database > database-dump.db
    scp database-dump.db user@host:/path/to/

e sul server si ripristina con::

    pg_restore -c -d database database-dump.db
