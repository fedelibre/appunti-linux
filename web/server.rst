.. _OpenSSH: http://www.openssh.org/
.. _CEST: http://it.wikipedia.org/wiki/Central_European_Summer_Time

******
Server
******

OpenSSH
=======

`OpenSSH`_ permette di generare chiavi crittografiche e collegarsi in
modo sicuro a macchine remote.  Su un client basta installare il
pacchetto ``openssh-client`` mentre sul server serve anche ``openssh-server``.

Per generare una coppia di chiavi con chiave RSA::

    ssh-keygen -t rsa

Questo comando genera due file nella cartella predefinita ``~/.ssh``:

- id_rsa: la chiave privata (da custodire gelosamente)
- id_rsa.pub: la chiave pubblica (da condividere all'esterno per permettere
  agli altri di poter crittare i messaggi a noi indirizzati).

Il file authorized_keys invece contiene una lista delle chiavi che hanno
diritto di accesso alla macchina, quindi ha senso soprattutto su un server.
Per caricare una chiave pubblica su un server si usa il comando::

    ssh-copy-id -i ~/.ssh/id_rsa.pub  utente@dominio

In questo modo si può accedere al server senza dover digitare la password
con questo semplice comando::

    ssh utente@dominio

Ovviamente se l'IP non è associato a un dominio occorre usare l'IP.

Se si gestisce un server è bene conoscere le migliori pratiche per
garantirne la sicurezza:

- http://www.tecmint.com/5-best-practices-to-secure-and-protect-ssh-server/
- http://www.cyberciti.biz/tips/linux-unix-bsd-openssh-server-best-practices.html

Conviene installare `fail2ban` e disabilitare l'accesso come root,
modificando il file ``/etc/ssh/sshd_config``.  Va riavviato il
servizio con::

  /etc/init.d/ssh restart

Il file di log degli accessi SSH è ``/var/log/auth.log``.



Ora
===

Conviene avere l'ora del server sincronizzata con la propria ora, in modo da
poter leggere con più facilità i log.
Di solito il fuso orario si configura in fase di installazione, ma nel caso
dei server è più facile trovarsi di fronte a un sistema già installato e
impostato probabilmente sul fuso orario americano (EDT)::

    $ cat /etc/timezone
    America/New_York
    $ date
    Tue Jul 31 06:53:15 EDT 2012

In Italia abbiamo il fuso `CEST`_, che si imposta così::

    # dpkg-reconfigure tzdata

    Current default time zone: 'Europe/Rome'
    Local time is now:      Tue Jul 31 12:55:07 CEST 2012.
    Universal Time is now:  Tue Jul 31 10:55:07 UTC 2012.


Infine si installano i pacchetti che aggiornano l'ora automaticamente (anche
quando si passa dall'ora legale a quella solare e viceversa)::

    aptitude install ntp ntp-doc ntpdate



Hostname
========

L'hostname è il nome che identifica la macchina ed è utile perché compare
nel prompt del terminale dopo l'utente, nella forma `utente@hostname`.
Se si hanno più schede aperte in un unica finestra di terminale, un corretto
hostname impedisce di confondersi.  Per rinominarlo in modo permanente basta
modificare il file ``/etc/hostname`` e riavviare il servizio::

    /etc/init.d/hostname start


Dal successivo login comparirà il nuovo hostname sul terminale.



DNS
===

Bisogna impostare i record DNS di un dominio in modo che puntino al giusto
indirizzo IP.  In caso di variazione, di solito i server DNS si aggiornano
entro un'ora.  Per controllare i record DNS si usa ``dig`` (domain information
groper).
Per verificare che l'IP sia stato aggiornato::

    dig dominio.it

Infatti, se non si specificano opzioni, dig controlla solo il record A (address),
ovvero l'IP associato al dominio.  Per vedere tutti i record DNS::

    dig dominio.it ANY


Se la posta è gestita da un server diverso da quello del dominio, occorre
abilitarlo in modo che le email non vengano considerate come spam.  A questo
servono i record SPF.

Approfondimenti:

- http://en.wikipedia.org/wiki/List_of_DNS_record_types
- http://www.openspf.org/SPF_Record_Syntax
- http://support.google.com/a/bin/answer.py?hl=it&answer=178723



Postfix
=======

`Postfix <http://www.postfix.org/>`_ è un MTA facile da configurare.
L'installazione rimuove exim, l'MTA predefinito di Debian::

    aptitude install postfix

È bene seguire la procedura guidata di configurazione, che permette di creare
un file di configurazione già funzionante (se si risponde bene alle domande).
Si sceglie la procedura "Internet Site" e si seguono le istruzioni.  Per ripetere
la procedura guidata in un secondo momento::

    dpkg-reconfigure postfix


I file di configurazione si trovano in ``/etc/postfix``.  I più importanti
sono master.cf e main.cf (il file che si può personalizzare).  Ogni volta che
si modifica main.cf, per rendere attive le modifiche si ricarica postfix::

    postfix reload

``postconf`` stampa l'elenco di tutte le opzioni di configurazione::

    postconf | less
    postconf | grep opzione



Alias
-----

Gli alias si aggiungono in ``/etc/aliases``, con questa sintassi::

    alias: utente
    aliasdue: utente@dominio.it, utente2@dominio.it, utente3@dominio.com

Per rendere effettive le modifiche servono due comandi::

    newaliases
    postfix reload

Consultare il manuale per maggiori informazioni: ``man aliases``.



Debug
-----

Per rintracciare eventuali problemi si guarda nei log::

    cat /var/log/mail.log


Riferimenti:

- http://www.postfix.org/documentation.html
- http://www.postfix.org/BASIC_CONFIGURATION_README.html
- http://guide.debianizzati.org/index.php/Mail_Server


