********************
Espressioni regolari
********************

regexp
======

Le regexp sono uno strumento molto utile per poter modificare tanti file in
un colpo solo e per la programmazione in generale.

Per testare le regexp uso un programma presente nei repository Debian:
`regexxer <http://regexxer.sourceforge.net/>`_.
Esistono anche vari strumenti online, ad esempio:

- http://rejex.heroku.com/
- http://gskinner.com/RegExr/

Le regexp sono spesso usate con dei programmi a linea di comando, come grep,
sed e rename.


grep
====

grep permette di individuare le stringhe di testo presenti in uno o più file.

Conviene evidenziare la stringa "matchata" tramite l'opzione ``--color=auto`` oppure
aggiungendo ``export GREP_OPTIONS='--color=auto'`` a ~/.bashrc.
Fonte: http://www.debian-administration.org/articles/460

Un esempio::

    grep -e 'first\b' *.ly


sed
===

`sed <http://www.gnu.org/software/sed/manual/>`_ usa le espressioni regolari
per manipolare le stringhe dei file di testo.  Il carattere di escape è ``\``.
La sintassi del comando è::

    sed OPZIONI 'comando/regexp/sostituzione/flag' INPUTFILE

È importante conoscere le funzioni dei comandi (s per sostituire) e dei flag.
Vediamo un esempio::

    sed -r 's/\\first/\\upper/' *.ly

Il risultato della sostituzione viene stampato sul terminale.  Quando si è
sicuri del comando si aggiunge l'opzone -i, che modifica il file.


rename
======

``rename`` funziona secondo la stessa logica di sed, ma opera sui nomi dei
file.  Vediamo alcuni esempi comuni.

Sostituisce tutti i caratteri maiuscoli con quelli minuscoli::

    rename 'y/A-Z/a-z/' *


Sostituisce tutti gli spazi con un altro carattere (ad esempio _)::

    rename 's/\ /_/g' *


