.. _Idle: http://www.python.org/idle
.. _ClearWindow: http://bugs.python.org/issue6143
.. _PyPI: http://pypi.python.org/pypi
.. _Virtualenv: http://www.virtualenv.org/

******
Python
******

Risorse
=======

Corsi online:

- `Programmare in Python 2 <http://www.youtube.com/playlist?list=PLC4DEDAECF24B855D>`_
  (videocorso di Marco Beri)
- http://www.codecademy.com/
- https://www.coursera.org/course/programming1


Libri:

- http://www.python.org/community/sigs/current/edu-sig/
- http://rmi.net/~lutz/about-lp4e.html
- http://pragprog.com/book/gwpy2/practical-programming
- http://www.greenteapress.com/thinkpython/
- http://csunplugged.org/

Screencast:

- http://pyvideo.org/


Editor e IDE
============

Liberi (quasi tutti nei repo Debian):

* `http://eric-ide.python-projects.org/ <Eric>`_
* `http://ninja-ide.org/ <ninja-ide>`_
* `https://code.google.com/p/spyderlib/ <spyder>`_
* `http://www.vim.org/ <vim>`_
* `https://github.com/fortharris/Pcode <Pcode>`_ (dipendenze: python3-pyqt4.qsci python3-pyqt4)

Proprietari:

* `http://www.jetbrains.com/pycharm/] <PyCharm>`_ (specifico anche per Django)
* `http://www.sublimetext.com/ <Sublime>`_
* `http://wingware.com/ <Wing IDE>`_


Idle
----

`Idle`_ è un IDE per Python molto usato a scopo didattico.  Non è indicato
per lo sviluppo vero e proprio, ma bisogna imparare a usarlo se si vogliono
seguire i libri e i corsi dedicati a Python.

Scorciatoie utili:

- Alt-p » comando precedente
- Ctrl-l » pulisce lo schermo (se è installata l'estensione `ClearWindow`_)

Le estensioni di Idle sono dei file `.py` che devono essere copiati nella
directory ``idlelib`` della versione di Idle che si sta usando.  Si può
cercare così::

    # find /usr/lib -name 'idlelib'
    /usr/lib/python3.2/idlelib

Basta copiare le linee indicate all'inizio dell'estensione nel file
``config-extensions.def`` di quella directory.
In realtà sarebbe meglio definirle in ``~/.idlerc``, come spiegato nel
file ``idlelib/config-main.def``.  Ma la scorciatoia non funziona.



Virtualenv
==========

`Virtualenv`_ è uno strumento di cui non si può fare a meno nello sviluppo
di un progetto in Python.  Serve a isolare l'ambiente di sviluppo e a evitare
conflitti tra applicazioni che richiedono versioni diverse di uno stesso
software.

Si può installare da repository Debian o dal PyPI (meglio il secondo)::

    pip install virtualenv


Il funzionamento è banale::

    virtualenv percorso/alla/directory              # crea il virtualenv
    source percorso/alla/directory/bin/activate     # lo attiva
    deactivate                                      # lo disattiva


.. NOTE::
   Il nome della directory dell'ambiente virtuale comparirà all'inizio del
   prompt del terminale per ricordarci che siamo dentro al virtualenv.
   Conviene quindi usare un nome che identifichi il progetto per il quale
   si è creato il virtualenv.


Uno strumento comodo per attivare più comodamente un ambiente virtuale è
``virtualenvwrapper``::

    pip install virtualenvwrapper

Quindi si crea la directory nascosta ``~/.virtualenvs`` dove verranno salvati
i virtualenv creati, e si aggiungono queste linee a ``~/.bashrc``::

    export WORKON_HOME=$HOME/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh
    export PIP_VIRTUALENV_BASE=$WORKON_HOME

Questo semplifica anche la creazione dei virtualenv perché non è più
necessario specificare il percorso::

    mkvirtualenv nome-ambiente

L'ambiente sarà salvato automaticamente in ``~/.virtualenv`` e avviato.
I comandi utili sono::

    deactivate            # per uscire da un ambiente
    workon                # per vedere l'elenco degli ambienti installati
    workon nome-ambiente  # per attivare un ambiente

Per entrare nella directory del virtualenv ci sono due modi::

    cdvirtualenv       # entra nella directory definita nella variabile $VIRTUAL_ENV
    cdproject          # se si è eseguito setvirtualenvproject nella directory del progetto


Riferimenti:
http://virtualenvwrapper.readthedocs.org/en/latest/command_ref.html

Il nome dell'ambiente deve essere il più specifico possibile, in
modo da identificare un solo progetto.  Ad esempio ``django-pippo``.
Per rimuovere un ambiente basta eliminare la sua directory e non sarà
più mostrato da ``workon``.

Quando si sviluppa conviene sapere quali sono i pacchetti python necessari,
in modo da poter riprodurre l'ambiente di sviluppo su altre macchine.
Ovviamente non bisogna fare il controllo di versione dei pacchetti veri e
propri, ma solo dell'elenco.  Per farlo si usa ``pip``.

Alcune applicazioni basano il loro funzionamento su delle variabili d'ambiente
di sistema, come ad esempio il locale (la lingua).  Mi è capitato ad esempio
di avere problemi con Mezzanine perché il locale del virtualenv non aveva
l'UTF-8 e quindi non potevo visualizzare una pagina che caricava dei file
con caratteri accentati.  Sembra che basti definire la nuova variabile una
volta e questa viene salvata da qualche parte.  Per esempio::

    export LC_ALL=it_IT.UTF-8

Può capitare anche di dover aggiornare la versione di Python installata
nell'ambiente virtuale, magari a causa di un bug.  Con questo comando
si aggiorna anche una versione minore (per esempio da 2.7.3 a 2.7.4, se
a livello di sistema la versione è stata aggiornata alla 2.7.4)::

    virtualenv --no-site-packages -p python2.7 <virtualenv>



Pip
===

``pip`` è il migliore strumento per installare i pacchetti presenti
nel `PyPI`_, un repository di software scritto in python.  Ecco i
comandi più utilizzati::

    pip install pacchetto
    pip install --upgrade pacchetto
    pip uninstall pacchetto
    pip search pacchetto

Se si lavora molto con gli ambienti virtuali (consigliato), conviene impostare
una directory cache in cui salvare i pacchetti scaricati, in modo
da evitare download multipli.  Si crea la directory ``~/.pip/download-cache``
e si aggiungono queste righe a ``~/.pip/pip.conf``::

    [global]
    --download-cache=/home/fede/.pip/download-cache

.. NOTE::
   Il percorso deve essere assoluto e completo. Le variabili d'ambiente
   non funzionano.

Abbiamo visto che conviene isolare ogni progetto python nel suo
ambiente virtuale, per evitare conflitti coi pacchetti di sistema.
Per poter lavorare sullo stesso progetto in un altro computer
bisogna sapere quali sono i pacchetti necessari.  Basta salvare
i programmi installati da pip con questo comando::

    pip freeze > requirements.txt

Sul nuovo computer si installeranno i pacchetti con questo comando::

    pip install -r requirements.txt    # su un altro computer

Documentazione: http://www.pip-installer.org/en/latest/index.html



Django
======

Tutorial:

- http://www.jeffknupp.com/blog/2012/02/09/starting-a-django-project-the-right-way/
- https://docs.djangoproject.com/en/dev/
- https://code.djangoproject.com/wiki/Tutorials
- http://gettingstartedwithdjango.com/
- http://www.tangowithdjango.com/book/

Creare la root del progetto e verificare il funzionamento del server::

    django-admin.py startproject project_name
    cd project_name
    ./manage.py runserver



Se si usa South, al posto di syncdb si dovranno usare questi comandi ogni
volta che si vuole generare il database (dopo aver modificato il modello
dell'applicazione):

    ./manage.py schemamigration applicazione --auto
    ./manage.py migrate applicazione


- http://south.readthedocs.org/en/latest/tutorial/part1.html



Applicazioni Python interessanti
================================

- https://github.com/jeffknupp/blug
- http://getpelican.com/ (generatore di blog statici)

