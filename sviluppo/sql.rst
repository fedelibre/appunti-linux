***
SQL
***

Concetti basilari dei database relazionali:

- http://boasso.net/ecdl/corso_modulo5_base/index.htm
- https://code.tutsplus.com/courses/sql-essentials
- http://dev.mysql.com/doc/index.html

Possiamo immaginare un database come un foglio di calcolo dove:

- ogni scheda equivale a una **tabella**, composta da colonne e righe, contenente
  i dati specifici di un argomento;
- le colonne indicano i **campi** compresi in quella tabella, ciascuno
  dei quali avrà uno specifico *datatype*;
- le righe corrispondono a ogni **record** della tabella e contengono
  i valori associati a ciascun campo;
- diversi elementi potranno avere lo stesso valore; per identificare un elemento
  di una tabella conviene definire la **chiave primaria**.  È questa che
  permette di mettere in relazione le diverse tabelle e estrarre i dati.

In questa pagina seguo le convenzioni tipiche dei manuali bash:

- <nome> è una variabile
- [<opzione>]: tra parentesi quadre si trovano gli argomenti opzionali



MySQL
=====

Per lavorare su un database MySQL occorre installare il pacchetto server::

    sudo aptitude install mysql-server

Durante l'installazione viene chiesta la password di root per accedere
al database.  Per connettersi al server MySQL si usa questo comando::

    mysql -u root -p

I comandi SQL possono essere scritti sia in maiuscolo che in minuscolo
ma per convenzione si scrivono in maiuscolo per distinguere chiaramente
i comandi dai nomi di database, tabelle, etc.  Devono terminare con ``;``,
altrimenti il comando si aspetta altre istruzioni su una nuova riga.
I più usati sono::

    mysql> SHOW DATABASES;
    mysql> CREATE DATABASES <database>;
    mysql> DROP DATABASE <database>;
    mysql> USE <database>;
    mysql> CREATE TABLE <tabella> (
    -> <campo> varchar(100),
    -> <campo> varchar(30),
    -> <campo> integer);
    mysql> show tables;
    mysql> EXPLAIN <tabella>;

Per inserire i dati in una tabella si può usare il metodo implicito
o, preferibilmente, quello esplicito::

    mysql> INSERT INTO <tabella> VALUES("stringa", "stringa", 4);
    mysql> INSERT INTO <tabella>(<campo>, <campo>, <campo>)
        -> VALUES("stringa", "stringa", 4);

Per estrarre i dati da una tabella::

    mysql> SELECT * FROM <table>;
    mysql> SELECT <campo>, <campo> FROM <table>;

Conviene definire una **chiave primaria**, che non è altro che una colonna
della tabella il cui valore deve essere diverso in ogni riga; questo permette
di identificare ogni record della tabella in modo univoco e quindi interrogare
tabelle diverse del database che si riferiscono allo stesso id.  Ecco un esempio
di come impostare la chiave primaria::

    mysql> CREATE TABLE <tabella> (
        -> id INT NOT NULL AUTO_INCREMENT,
        -> <campo> varchar(100),
        -> <campo> varchar(10),
        -> <campo> date,
        -> CONSTRAINT <tabella_pk> PRIMARY KEY (id));

Abbiamo creato una tabella la cui colonna ``id`` viene impostata come
chiave primaria.  CONSTRAINT fa sì che ogni record aggiunto debba
avere id anche se non viene specificato, mentre AUTO_INCREMENT fa sì
che il numero venga generato automaticamente se non viene specificato.

Nella vita reale un ottimo esempio di chiave primaria è il codice fiscale:
ci possono essere persone con lo stesso nome e cognome, ma il codice
fiscale sarà diverso ed è quindi un buon strumento per identificare una
singola persona in un database.

Per modificare una tabella esistente si usa il comando *ALTER*::

    ALTER TABLE <vecchio-nome> RENAME <nuovo-nome>;
    ALTER TABLE <tabella> ADD <campo> [<tipo> <opzioni>];
    ALTER TABLE <tabella> DROP <campo>;
    ALTER TABLE <tabella> CHANGE <campo-vecchio> <campo-nuovo> <tipo>;

Per eliminare tutti i campi di una tabella, mantenendo la tabella::

    DELETE FROM <tabella>;

Per eliminare la tabella (compreso il suo contenuto)::

    DROP TABLE <tabella>;

Per aggiornare un record si usa questa sintassi::

    UPDATE <table> SET <col> = <val> [<condizioni>];

Se non si specifica alcuna condizione il cambiamento avrà effetto
su tutti i record.

Può essere utile definire i comandi in un file di testo .sql
da eseguire in un terminale, per creare il database, le tabelle
e inserire i record, come in
`questo esempio <https://gist.github.com/andrew8088/3057504>`_.
Si apre un terminale e si esegue uno di questi comandi::

    $ mysql <database> < file.sql
    $ mysql < file.sql

Il secondo comando è possibile se il file SQL contiene il comando
``USE <database>;``.

Si può eseguire uno script anche all'interno della shell SQL::

    mysql> source file.sql

Per restringere il campo di azione di un qualsiasi comando (SELECT,
UPDATE, etc.) si può usare il comando **WHERE**, seguito dai filtri
stabiliti tramite operatori di confronto e logici::

    SELECT * FROM <tabella> WHERE <campo> = "testo" AND <campo2> <= 3;

Operatori di confronto: =, >, <, >=, <=, <> o != (entrambi
significano non uguale a).

Operatori logici: ALL, AND, ANY, BETWEEN, EXISTS, NOT, OR, SOME,
IN, LIKE.

In SQL il carattere ``%`` è equivalente al ``*`` in bash, quindi
``LIKE "055%"`` mostra tutti i record che iniziano con 055.

**LIMIT** serve a limitare il numero di risultati di una query.
**OFFSET** serve a spostare il punto in cui inizia a contare
per determinare quanti record mostrare.
**DISTINCT** permette di escludere dalla ricerca i risultati "doppi".

Per cambiare l'ordine dei risultati di una query si usa **ORDER BY**,
specificando uno o più campi e eventualmente l'ordine ascendente
o discendente::

    SELECT * FROM <tabella> ORDER BY <campo>;
    SELECT * FROM <tabella> ORDER BY <campo> ASC;
    SELECT * FROM <tabella> ORDER BY <campo> DESC;
    SELECT * FROM <tabella> ORDER BY <campo> ASC, <campo2> DESC;

**GROUP BY** si usa in combinazione con funzioni di aggregazione (come
AVG, MAX, MIN, SUM e COUNT) per raggruppare i risultati in base a
un campo::

    SELECT <campo>, AVG(<campo2>) FROM <tabella> GROUP BY <campo>;



Query complesse
===============

Nella maggior parte dei casi si ha necessità di lanciare una query
che estragga e connetta i dati presenti in più tabelle.  I risultati
di una query non sono altro che una nuova "tabella" i cui elementi
sono definiti dalla combinazione di altre tabelle.
Ci sono due modi: le subquery e i join.

Le *subquery* sono query annidate::

    SELECT * FROM <tabella> WHERE <campo> = (SELECT <campo2> FROM <tabella2> WHERE <campo3> = "testo");

Oppure si possono unire due query insieme con **UNION** (bisogna
selezionare lo stesso numero di colonne in ciascuna istruzione
SELECT)::

    SELECT first_name, last_name FROM detectives UNION SELECT first_name, last_name FROM criminals ORDER BY last_name;

I join sono un altro strumento.
Se voglio unire tutte le colonne di una tabella con tutte le colonne
di un'altra tabella::

    SELECT * FROM <tabella> CROSS JOIN <tabella2>;

Se uso i join devo fare attenzione a specificare le tabelle in cui si
trovano colonne che hanno lo stesso nome in tabelle diverse::

    SELECT detectives.last_name, criminals.last_name FROM detectives CROSS JOIN criminals CROSS JOIN cases WHERE cases.criminal_id = criminals.id AND cases.detective_id = detectives.id;

Funzioni e alias
----------------

Le funzioni e gli alias danno la possibilità di creare risultati personalizzati.
La funzione CONCAT unisce insieme i valori di colonne o delle stringhe.
L'alias permette di "rinominare" o meglio di mostrare una colonna
con un nome diverso da quello reale::

    SELECT titolo, autore AS compositore FROM brani;

La colonna autore apparirà come compositore nei risultati della query.

Il comando AS può essere utile in casi più complessi, ad esempio per
migliorare il nome di una colonna::

    SELECT CONCAT(nome, " ", cognome) AS nome FROM persone;

Se non si usasse ``AS nome`` il risultato mostrerebbe la funzione CONCAT.
Oppure per ridurre l'input di un cross join in cui vogliamo estrarre
la colonna nome presente in due diverse tabelle::

    SELECT p.nome FROM persone AS p CROSS JOIN case;

Un altro esempio di funzione è COUNT, che conta il numero di occorrenze
di una certa selezione::

    SELECT COUNT(*) FROM <tabella> WHERE <colonna> LIKE "055%";


