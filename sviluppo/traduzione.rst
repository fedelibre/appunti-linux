.. _gettext: http://www.gnu.org/software/gettext/
.. _gTranslator: http://projects.gnome.org/gtranslator/
.. _Translation Project: http://translationproject.org/
.. _GnomeUtils: https://live.gnome.org/GnomeUtils

**********
Traduzione
**********

Editor per file .po (gettext)
=============================

La maggior parte dei progetti di software libero usa `gettext`_
per gestire le traduzioni.

Per poter contribuire come traduttore, di norma è sufficiente
conoscere bene il funzionamento di un editor di file .po come
PoEdit o gTranslator.

`gTranslator`_ è l'editor che si usa in Gnome.  All'avvio si
imposta il profilo utente che verrà poi inserito nell'header del
file: nome, email personale, email del gruppo di traduzione e
forma del plurale (nplurals=2; plural=(n!=1)).

Conviene attivare alcuni utili plugin:

- *Open-Tran*, che cerca le traduzioni nel database di http://it.en.open-tran.eu/
- *Visualizza codice sorgente*, che permette di aprire il file
  del codice sorgente da cui è stata generata la stringa, in
  modo da capire il contesto del messaggio.  È necessario che
  il file .po si trovi nella directory principale dei sorgenti
  e che i link siano assoluti (altrimenti non troverà il file).

Attualmente gTranslator non supporta l'aggiornamento del file .po
a partire da un file .pot.  Per farlo si può usare Poedit, oppure
la linea di comando::

    cd <progetto>/po
    intltool-update it

.. NOTE::
   Se il progetto di traduzione è gestito dal `Translation Project`_,
   il file po aggiornato viene creato automaticamente e, se
   si vuole, spedito automaticamente per email.  Se per qualche
   ragione si volesse aggiornare un file da soli, assicurarsi di
   usare come file .pot di riferimento quello presente sul sito
   del TP e non quello dei sorgenti (che potrebbe essere cambiato
   nel frattempo).

Per vedere le traduzioni nel contesto del programma, bisogna
prima generare il file binario XX.gmo a partire da XX.po::

    msgfmt -v it.po -o it.gmo

.. NOTE::
   Il file di output non dovrebbe essere necessario, ma mi è successo che
   msgfmt non producesse alcun file .gmo se non lo specificavo.

Nei progetti in cui ho contribuito di solito c'è un Makefile apposito::

    cd <progetto>
    make -C po update-po
    make
    make install


Scorciatoie in gTranslator
--------------------------

Scorciatoie da tastiera molto utili:

- ``Alt + →`` / ``Alt + ←`` per passare alla stringa successiva/precedente
- ``Ctrl + spazio`` per copiare il messaggio originale
- ``Ctrl + U`` per attivare/disattivare lo stato fuzzy

.. NOTE::
   Nelle preferenze di gTranslator c'è l'opzione "Rimuovere lo stato
   fuzzy se il messaggio è stato modificato". Questa impostazione di
   solito conviene, ma se si vuole mandare in revisione un file può
   convenire disattivarla così che le stringhe da controllare siano
   facilmente visibili. Lo stato fuzzy si può togliere dopo con
   Ctrl + U.



Dizionari
=========

Il dizionario di Gnome, che fa parte del metapacchetto `GnomeUtils`_ può usare
sia risorse online che dizionari installati localmente.  Il pacchetto
italiano/inglese è ``dict-freedict-eng-ita``.

Dizionari online:

- http://www.pylyglot.org/
- http://open-tran.eu/
