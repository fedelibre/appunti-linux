**********
PostgreSQL
**********

`PostgreSQL <http://www.postgresql.org/>`_ è presente nei repository::

    aptitude install postgresql postgresql-doc pgadmin3

``pgadmin3`` è il front-end grafico.
La documentazione è in:
file:///usr/share/doc/postgresql-doc-x.y/html/index.html

La gestione di un database PostgreSQL è possibile sia tramite dei comandi
shell che tramite psql, il prompt di PostgreSQL.


Comandi shell
=============

I principali comandi (shell) di gestione del database sono:

- ``createuser|dropuser``: crea/elimina un nuovo utente PostgreSQL (o meglio, un ruolo)
- ``createdb|dropdb``: crea/elimina un nuovo database
- ``pg_dump``: fa un backup
- ``pg_restore``: crea un nuovo database a partire da uno salvato

L'elenco completo si trova nell'appendice della documentazione.

L'operazione tipica è fare il dump di un database e poi copiarlo in uno
nuovo::

    pg_dump -Fc dbname > db.dump
    createdb newdbname
    pg_restore -d newdbname db.dump

``createuser <utente>`` senza opzioni creerà un ruolo senza alcun privilegio.
Quindi conviene usare subito un'opzione che assegni qualche privilegio:

- ``-d`` permette di creare database
- ``-r`` permette di creare nuovi ruoli
- ``-s`` assegna i privilegi di superutente
- ``-g <ruolo>`` assegna tutti i permessi concessi al ruolo specificato
- ``-P`` se si vuole usare l'autenticazione con password

Oppure usare la modalità interattiva::

    createuser --interactive <user>

Come minimo, di norma conviene usare almeno ``-d``.


psql
====

.. NOTE::
   I comandi lanciati all'interno del prompt di postgresql si riconoscono
   perché iniziano col backslash oppure sono tipici comandi SQL.

L'utente  di sistema che gestisce i database PostgreSQL non può essere
root (per motivi di sicurezza) e di default è `postgres`.  Quindi
per accedere al prompt dei comandi di postgres (comando ``psql``)
occorre diventare l'utente postgres con su o sudo::

    su postgres -c 'psql'
    sudo -u postgres psql

Comandi utili nel prompt di postgres (psql):

- ``\h``: aiuto comandi
- ``\q``: uscire dal prompt
- ``\list``: elenca i database
- ``\du``: elenca i ruoli esistenti con i permessi
- ``\password <utente>``: password per l'utente...

I database vengono salvati nella directory ``/var/lib/postgresql/x.y/main``.

Il prompt di psql indica il database a cui si è connessi e i privilegi
dell'utente::

    postgres=#  ## connesso al datbase postgres con utente superuser
    test=>      ## connesso al database test con utente normale

Se lanciato senza specificare il nome del database a cui connettersi, psql
cerca di connettersi al ruolo e al database corrispondenti all'utente shell
con cui è stato lanciato psql; se non presenti, dà errore.


Aggiornamento di versione
=========================

L'aggiornamento del pacchetto debian dovrebbe gestire correttamente il
passaggio a una nuova versione.  Mi è capitato però che qualcosa andasse
storto e che in ``/etc/postgresql`` ci fosse solo la directory della
vecchia versione.  Dato che per fare l'upgrade è necessario avere
anche la versione vecchia, se non è più nei repository bisogna installare
due pacchetti .deb da snapshot.debian.org.  Quindi si esegue questo
comando come root::

    pg_upgradecluster 9.3 main

Dove gli argomenti sono la vecchia versione e il nome del cluster (il
predefinito è main).  L'aggiornamento viene fatto automaticamente fino
all'ultima versione installata.

