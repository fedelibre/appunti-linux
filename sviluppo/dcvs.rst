.. _DCVS (Distributed Control Version System): http://en.wikipedia.org/wiki/Distributed_revision_control

*********************
Controllo di versione
*********************

I `DCVS (Distributed Control Version System)`_ più utilizzati sono Git e Mercurial.


Git
===

Risorse:

- http://progit.org/
- http://gitref.org/


Scaricare un repository remoto
------------------------------

Ci sono vari modi per scaricare sul proprio computer i contenuti di un
repository Git remoto.
Il più semplice consiste nel clonare l'intero repository.  Ad esempio::

   git clone git://indirizzo.org/progetto.git

Se si vuole usare un branch in particolare, si stampa l'elenco dei
branch remoti e si fa il checkout del branch che ci interessa:

    git branch -a
    git checkout branch


A volte ci può servire una revisione ben precisa, ad esempio quella
che coincide col rilascio di una versione, solitamente collegata a
un tag.  Conviene creare un branch locale collegato al tag, per
facilitare il passaggio a altri branch/tag::

    git tag -l
    git checkout -b nuovo_branch_locale nome_tag


Creare un repository Git
------------------------

Vediamo come creare un repository Git su un server personale.  Per prima
cosa si crea un utente apposito sul server, dato che un repository viene usato
spesso da più persone e si vorrà isolare l'accesso::

    adduser git

Quindi, come utente git, si crea un repository vuoto nella home::

    su git
    cd
    mkdir progetto && cd progetto
    git init --bare


.. NOTE::
   Perché conviene usare un repository vuoto sul server:

   - http://gitready.com/advanced/2009/02/01/push-to-only-bare-repositories.html
   - http://git-scm.com/book/ch4-2.html

Ora si passa al computer in locale.  Per prima cosa si invia la propria
chiave pubblica nella home dell'utente git::

    ssh-copy-id -i git_rsa.pub git@server

Quindi si aggiunge il server remoto al proprio progetto Git (da creare), usando
il protocollo ssh::

    mkdir progetto && cd progetto
    git init
    git remote add origin ssh://git@server/home/git/progetto

Dopo aver committato i file in locale, si inviano al server con::

    git push origin master

.. NOTE::
    La prima volta è necessario specificare sia l'alias del server remoto (origin)
    sia il branch attuale (master).  In seguito non è necessario, se c'è un
    solo branch.




Commit
------

Per comprendere i commit bisogna sapere che i file in Git possono trovarsi in
tre stati:

- **untracked**: quando il file non è ancora stato aggiunto all'indice
- **modified**: quando un file già aggiunto viene modificato
- **staged**: quando un file modificato viene preparato per il commit


Le opzioni possibili sono:

- ``git commit``: aggiunge al commit solo i file "staged"
- ``git commit -a``:: aggiunge al commit anche i file "modified" e "untracked"

In altre parole, l'opzione -a può essere utile per saltare un comando (``git add``)
e soprattutto per essere sicuri di inserire nel commit tutte le modifiche.
Infatti se un file aggiunto per il commit viene modificato, da "staged"
torna nello stato "modified".

D'altra parte l'opzione -a va evitata se si vogliono aggiungere solo alcuni
file o solo i file modificati.  Per evitare di aggiungere i file untracked
c'è questo utile comando::

    git add -u .
    git commit

Spesso si ha necessità di aggiungere delle modifiche all'ultimo commit
creato, ad esempio in seguito a una revisione di altri sviluppatori.
In questo caso si fa la modifica, si aggiunge il file e poi si committa
con l'opzione ``--amend``::

    git commit --amend

Un'altra funzione utile (per esempio se il progetto, come live-build di
Debian, genera molte directory che sono escluse in .gitignore) può essere
stampare una lista dei file aggiunti all'indice::

    git ls-tree -r master --name-only


Modifiche
---------

Per conoscere l'ultimo commit della propria copia locale, che identifica
quindi la versione su cui si sta lavorando::

   git rev-list HEAD |head -1


Per annullare delle modifiche non ancora committate::

   git checkout <percorso>/file


Per annullare un commit::

   git revert <committ>


Per rimuovere tutti i file "untracked" si usa il comando ``git clean``.  Se
lanciato senza opzioni dovrebbe eliminare tutti i file non tracciati.  Spesso
però si ottiene un errore.  Meglio quindi procedere con cautela con questi
due comandi::

   git clean -n    # mostra solo quello che toglierà
   git clean -f    # forza la pulizia


Infine, per salvare del lavoro fatto non ancora pronto per essere
committato, si usa ``git stash``:

- http://git-scm.com/book/en/Git-Tools-Stashing


Patch
-----

La patch viene generata col seguente comando::

   git format-patch origin

Questo comando genera una patch per ogni commit successivo al commit
identificato come origin.  Dunque se ci sono due commit genererà due
file .patch: 0001-commit1.patch e 0002-commit2.patch.

.. WARNING::
   Se si ha più di un branch è necessario specificare quale: ``git format-patch origin/branch``


Log
---

Comandi utili per vedere i log::

   git log <file>                   # mostra l'elenco di tutte le modifiche di un file.
   git show <id-commit>             # mostra le differenze di un commit
   git show --follow <id-commit>    # mostra tutta la storia di un file, anche se spostato o rinominato




Mercurial
=========

`Mercurial <http://mercurial.selenic.com/>`_ è un software di controllo di
versione distribuito molto popolare (secondo solo a Git).  È utilizzato da
vari siti che ospitano repository software, come `Google Code <http://code.google.com/intl/it-IT/projecthosting/>`_
e `Bickbucket <https://bitbucket.org/>`_.

Si installa con::

   aptitude install mercurial


.. NOTE::
   Anche se il pacchetto si chiama mercurial, l'eseguibile è ``hg``.

Doc in italiano:

* http://mercurial.selenic.com/wiki/ItalianTutorial
* http://gpiancastelli.altervista.org/


Clonare e aggiornare un repository
----------------------------------

Per clonare un repository il comando è::

   hg clone ssh://user@server/project


Per aggiornarlo::

   hg pull -u


hg status
---------

``hg status`` mi fa vedere la situazione:

- `?` indica che si tratta di un file nuovo (ancora non aggiunto al repository).
  Per aggiungerlo: ``hg add file``.  Per eliminarlo: ``...``.
- M indica che il file è stato modificato.  Si può committare con ``hg commit file``.
  O annullare con ``hg revert file``.  Questo secondo comando crea una copia
  del file modificato con estensione .orig, che è utile se si decide di reinserire
  la modifica (basta fare ``cp file.orig file``).  Questi file sono innocui
  perché non vanno aggiunti al repository, quindi devono sempre comparire col
  simbolo `?`.  Volendo si possono eliminare, ad esempio: ``rm locale/*.orig``.




Inviare una patch allo sviluppatore
-----------------------------------

Se si modifica un file esistente non c'è bisogno di aggiungerlo di nuovo (come
in Git).  Quindi ``hg add file`` si usa solo per i nuovi file.  Per creare
la patch basta committare le modifiche e poi creare un file .diff::

   hg commit file
   hg export tip > patch_file.diff


- http://mercurial.selenic.com/wiki/Workflows
