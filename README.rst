Appunti GNU/Linux
=================

Mantengo questi appunti per avere un rapido accesso a informazioni
che mi sono servite in passato.


Come generare i file HTML sul proprio computer
----------------------------------------------

Installa i pacchetti necessari::

    aptitude install git python-pip
    pip install Sphinx


Scarica i sorgenti e compila::

    git clone git@bitbucket.org:fedelibre/appunti-linux.git
    cd appunti-linux
    make html
    gnome-www-browser _build/html/index.html
