****
Odoo
****

`Odoo <https://www.odoo.com/>`_ è un ERP scritto in Python e rilasciato
con licenza AGPL.

- https://github.com/odoo/odoo
- http://www.odoo-italia.org/
- https://help.odoo.com/
- https://www.odoo.com/documentation/8.0/
- http://openerpbook.com/

Installazione
=============

Nonostante ci sia la possibilità di installare Odoo tramite i
`repository dei pacchetti .deb compilati ogni notte <http://nightly.odoo.com/>`_
(specificando la versione desiderata), lo sconsiglio per vari motivi:

#. non esiste una chiave GPG che verifichi l'autenticità del pacchetto
#. il pacchetto va comunque bloccato per evitare aggiornamenti continui
#. il pacchetto Debian non funziona *out of the box* e dunque è meglio
   usare i sorgenti, che forniscono anche più informazioni di debug
#. avere un servizio di init che lanci il server all'avvio può essere
   comodo ma è anche inutile

Quindi installiamo le dipendenze minime di sistema (database e le librerie
di sviluppo necessarie per compilare alcune dipendenze)::

    aptitude install postgresql python-dev libsasl2-dev

E cambiamo le autorizzazioni di accesso ai socket in
``/etc/postgresql/x.y/main/pg_hba.conf`` in modo che sia md5::

    # "local" is for Unix domain socket connections only
    local   all             all                                     md5

E riavviamo il server con ``service postgresql restart``.

Poi scarichiamo i sorgenti::

    git clone https://github.com/odoo/odoo

Conviene installare le dipendenze di Odoo in un ambiente virtuale, in modo
separato dal sistema::

    mkvirtualenv odoo
    cd <odoo-sorgenti>
    pip install -r requirements

Quindi si crea il file ~/.openerp_serverrc con questo comando::

    ./openerp-server -s

I parametri più importanti sono::

    db_port = 5433 # o il valore predefinito in /etc/postgresql/x.y/main/postgresql.conf
    db_name  = # di default corrisponde all'utente di sistema che lancia odoo
    db_password = # password dell'utente db_name con cui ci si connette al server database

.. NOTE::
   Conviene quindi creare un ruolo PostgreSQL con nome corrispondente all'utente
   di sistema che lancerà Odoo e definire la sua password.  Vedi :doc:`/sviluppo/postgres`.

Si rilancia openerp-server e Odoo dovrebbe essere pronto all'indirizzo
http://localhost:8069/

Se la porta dovesse essere occupata, si può verificare quale processo e
quale programma la sta usando con::

    sudo netstat -lptu




Uso
===

Al primo avvio si crea un database.  Se non si carica alcun dato di
prova preimpostato, bisogna installare qualche modulo per vedere
le principali funzionalità (altrimenti si vede solo il menu Impostazioni,
che dà accesso alla gestione dei moduli/app e degli utenti).

