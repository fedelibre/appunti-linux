****
Film
****

Ripping
=======

Per leggere i DVD criptati occorre la libreria libdvdcss, che si trova
soltanto su deb-multimedia.org: http://wiki.debian.org/it/MultimediaCodecs

Il pacchetto ``ogmtools`` contiene un programma che fa l'elenco dei
titoli e dei capitoli di un DVD::

    dvdchapx /dev/dvd

Il miglior programma di ripping è Handbrake, che permette di estrarre ogni
singolo titolo/capitolo su un singolo file, creando una coda di lavori.



Sottotitoli
===========

Ricerca dei sottotitoli
-----------------------

Il sito più gettonato è `opensubtitles.org <http://www.opensubtitles.org>`_.

`subdownloader <http://www.subdownloader.net/>`_ è un applicazione libera
che fornisce una comoda interfaccia a questo sito::

   aptitude install subdownloader

Sembra che possa bastare indicare la posizione del file video o della cartella
contenente i file video (VIDEO_TS nel caso di un DVD) perché il programma
capisca qual è il film in questione.  A me però non è mai funzionato.
Per fortuna c'è la ricerca nel sito opensubtitles.org

.. NOTE::
   Conviene usare i file caricati da utenti *trusted*.


Riproduzione
------------

Non basta trovare il file, bisogna verificare che funzioni correttamente.
Si lancia VLC, si apre il disco e si avvia il film.  Quindi si mette subito
in pausa e in Audio si seleziona la lingua originale e in Video»Traccia
sottotitoli»Apri file si apre il file dei sottotitoli.

Se invece si ha un file, basta chiamare il file .srt con la stessa radice
del nome del file video, ad esempio::

   |-- InvasioniBarbariche.en.srt
   |-- InvasioniBarbariche.fr.srt
   `-- InvasioniBarbariche.m4v

Se i sottotitoli non sono sincronizzati con le immagini, probabilmente il
file .srt è stato creato con impostazioni diverse.

subtitleeditor è in grado di riprodurre il file video (non il DVD, credo) per
vedere se i sottotitoli sono sincronizzati.  Oltre ai secondi ha anche i
centesimi.


Correggere il file .srt
-----------------------

Per fortuna esiste un comodissimo strumento per risincronizzare il file .srt::

   aptitude install subtitleeditor

http://home.gna.org/subtitleeditor/

La prima ragione per cui un file dei sottotitoli non è sincronizzato al video
è che è stato realizzato usando un filmato con una diversa frequenza di
fotogrammi (fps, frame per second).  Si può verificare guardando il valore
di Immagini al secondo in Strumenti»Informazioni codificatore.

I DVD pal dovrebbero avere 25 fps:
http://en.wikipedia.org/wiki/DVD-Video#Frame_size_and_frame_rate

Sembra che la maggior parte dei file .srt abbia un frame rate di 23.976,
che ovviamente non va bene con i 25 fps dei DVD.  In Subtitleeditor basta
andare in Timings»Change framerate.

Questa modifica dovrebbe già produrre un enorme miglioramento
nella sincronizzazione.  Ma se persiste un leggero ritardo si può
correggere con Timings»Muovi sottotitoli, con cui si può modificare
uno dei primi sottotitoli e di conseguenza tutti i successivi.

.. NOTE::
   Per avere risultati accurati conviene usare un lettore capace di indicare
   i millesecondi (ad esempio Avidemux), in modo da individuare il frame
   esatto su cui risincronizzare il file .srt


Riferimenti:

- http://www.debuntu.org/2006/04/19/30-how-to-synchronising-subtitles-with-subtitleeditor

